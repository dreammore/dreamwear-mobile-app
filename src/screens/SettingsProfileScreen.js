/* global require */
import React, { Component, useState, useCallback } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Platform,
  Text,
} from 'react-native';
import { Form } from 'native-base';
import PropTypes from 'prop-types';

import NavigationBar from '../elements/NavigationBar';
import PrimeButton from '../elements/PrimeButton';
import TextInput from '../elements/TextInput';
import SelectBox from '../elements/SelectBox';

import CommonStyles from '../styles/CommonStyles';
import {
  responsiveWidth,
  responsiveHeight,
  marginHorizontal,
  spaceVertical,
  btnHeight,
  inputHeight,
  NAV_HEIGHT,
  STATUSBAR_HEIGHT,
  deviceHeight,
  colors,
} from '../styles/variables';
import { settingAvaImg } from '../styles/image-variables';
import { cameraIc } from '../styles/icon-variables';

export default function SettingsProfileScreen(props) {
  const [subContLayout, setSubContLayout] = useState({
    height: null,
    width: null,
  });

  const onSubContLayout = useCallback((e) => {
    const { width, height } = e.nativeEvent.layout;
    setSubContLayout({
      width: width,
      height: height,
    });
  });

  const subContHeight = subContLayout.height;
  const btnContHeight =
    deviceHeight - (subContHeight + NAV_HEIGHT + STATUSBAR_HEIGHT);
  const btnSetting = {
    btnWidth: responsiveWidth(74.4),
    btnHeight: btnHeight,
  };

  const { navigation } = props;

  function handleTextInput(event) {
    const { name } = event.target.dataset;
    alert('Dsdsd');

    console.log('name' + name);
  }

  function handleOnPressUpdate() {}

  return (
    <View style={CommonStyles.container}>
      <NavigationBar back navigation={navigation} title="SETTINGS PROFILE" />
      <ScrollView>
        <View onLayout={onSubContLayout}>
          {/* add avatar start here */}
          <View style={styles.banner}>
            <Text small bold style={{ textAlign: 'center', color: '#FFF' }}>
              MODIFIER MON PROFILE
            </Text>
          </View>
          <View style={styles.banner_error}>
            <Text
              small
              bold
              style={{ textAlign: 'left', color: '#FFF', marginLeft: 10 }}>
              <Text>
                {' '}
                {'\n'}* Laisser le champ mot de passe vide pour conserver votre
                mot de passe actuel
                {'\n'}
              </Text>
            </Text>
          </View>
          {/* add avatar end here */}

          {/* setting form start here */}
          <Form style={styles.form}>
            <View style={styles.row}></View>
            <TextInput
              name={'last_name'}
              onChange={(event) => {
                console.log(name);
              }}
              inputHeight={inputHeight}
              label="Nom"
            />
            <TextInput
              data-name={'first_name'}
              inputHeight={inputHeight}
              label="Prénom"
            />
            <TextInput
              data-name={'display_name'}
              inputHeight={inputHeight}
              label="Nom affiché"
            />
            <TextInput
              data-name={'user_pass'}
              inputHeight={inputHeight}
              label="Mot de passe"
            />
            <TextInput
              inputHeight={inputHeight}
              label="Confirmez le mot de passe"
            />
          </Form>
          {/* setting form end here */}
        </View>

        {/* submit button start here */}
        <View
          style={[
            styles.btnCont,
            btnContHeight > btnHeight + spaceVertical.normal
              ? { height: btnContHeight, paddingBottom: spaceVertical.normal }
              : { marginBottom: spaceVertical.normal },
          ]}>
          <PrimeButton
            onPressButton={handleOnPressUpdate}
            navigation={navigation}
            setting={btnSetting}
            btnText="Valider"
          />
        </View>
        {/* submit button end here */}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  banner: {
    marginHorizontal: marginHorizontal.small,
    marginTop: spaceVertical.semiSmall,
    marginBottom: spaceVertical.small,
    paddingVertical: spaceVertical.small / 2,
    backgroundColor: colors.black,
  },
  banner_error: {
    marginHorizontal: marginHorizontal.small,
    paddingVertical: spaceVertical.small / 2,
    backgroundColor: colors.red,
  },
  avaCont: {
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    width: settingAvaImg.width,
    backgroundColor: colors.black,
    paddingTop: responsiveHeight(6),
  },
  ava: {
    width: settingAvaImg.width,
    height: settingAvaImg.height,
    borderRadius: settingAvaImg.height,
    overflow: 'hidden',
  },
  addAvaBtn: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  form: {
    marginHorizontal: marginHorizontal.large,
    paddingTop: spaceVertical.semiSmall,
    paddingBottom: responsiveHeight(6),
  },
  row: {
    flexDirection: 'row',
    width: 220,
  },
  btnCont: {
    alignItems: 'center',
  },
});

SettingsProfileScreen.propTypes = {
  navigation: PropTypes.any,
};
