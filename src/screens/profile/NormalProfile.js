/* global require */
import React, { Component, useState, useEffect, useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Image,
  Platform,
  ActivityIndicator,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Text from '../../elements/Text';
import Border from '../../elements/Border';

import ListItem from '../../components/ListItem';
import {
  responsiveWidth,
  responsiveHeight,
  marginHorizontal,
  spaceVertical,
} from '../../styles/variables';
import {
  notificationIc,
  orderIc,
  walletIc,
  outlineHeartIc,
  logoutIc,
} from '../../styles/icon-variables';
import { avaImg } from '../../styles/image-variables';
import { onRequestUserDetails } from '@actions/userActions';
import dmwlogo from 'img/dmw/logo.png';
import { Dimensions } from 'react-native';

export default function NormalProfile(props) {
  /*props */
  const { navigation } = props;
  const dispatch = useDispatch();

  const userId = useSelector((state) => state.loginReducer.id);

  const _state = useSelector((state) => state.userReducer.requestState);
  const userInfo = useSelector((_state) => _state.userReducer.userInfo);

  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;

  const renderLoader = () => {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          height: windowHeight - 150,
        }}>
        <Image
          source={dmwlogo}
          style={{
            width: 155,
            height: 22,
            marginBottom: 10,
          }}
        />
        <ActivityIndicator />
      </View>
    );
  };

  const renderErrorScreen = () => {};

  const renderProfilePage = (userInfo) => {
    return (
      <View>
        {/* user info start here */}
        <View style={styles.user}>
          <View style={styles.avatar}>
            <Image
              source={props.avatar}
              style={{
                width: avaImg.width,
                height: avaImg.height,
                ...Platform.select({
                  android: {
                    borderRadius: avaImg.width,
                  },
                }),
              }}
            />
          </View>
          <View style={{ marginLeft: marginHorizontal.small }}>
            <Text
              black
              medium
              mediumBold
              style={{ paddingBottom: responsiveHeight(1.2) }}>
              {userInfo.display_name}
            </Text>
          </View>
        </View>
        {/* user info end here */}

        {/* list item start here */}
        <Border bottom width={responsiveWidth(91.47)} alignSelf="flex-end" />
        <View style={styles.mainMenu}>
          <ListItem
            leftIcon={require('../../../img/icons/ic_profile.png')}
            leftIconStyles={{
              width: notificationIc.width,
              height: notificationIc.height,
            }}
            header="Details du compte"
            borderWidth={responsiveWidth(91.47)}
            onPressItem={() => navigation.navigate('SettingsProfileScreen')}
          />
          <ListItem
            leftIcon={require('../../../img/icons/ic_order.png')}
            leftIconStyles={{
              width: orderIc.width,
              height: orderIc.height,
            }}
            header="Mes commandes"
            borderWidth={responsiveWidth(91.47)}
            onPressItem={() => navigation.navigate('MyOrderScreen')}
          />
          <ListItem
            leftIcon={require('../../../img/icons/ic_location.png')}
            leftIconStyles={{
              width: 13,
              height: 18,
            }}
            header="Addresses"
            borderWidth={responsiveWidth(91.47)}
            onPressItem={() => navigation.navigate('AddressScreen')}
          />
          <ListItem
            leftIcon={require('../../../img/icons/ic_wallet.png')}
            leftIconStyles={{
              width: walletIc.width,
              height: walletIc.height,
            }}
            header="Moyen de Paiement"
            borderWidth={responsiveWidth(91.47)}
            onPressItem={() => navigation.navigate('PaymentScreen')}
          />
          <ListItem
            leftIcon={require('../../../img/icons/outline_ic_heart.png')}
            leftIconStyles={{
              width: outlineHeartIc.width,
              height: outlineHeartIc.height,
            }}
            header="Ma liste de souhait"
            borderWidth={responsiveWidth(91.47)}
            onPressItem={() => navigation.navigate('WishListScreen')}
          />
          <ListItem
            leftIcon={require('../../../img/icons/ic_logout.png')}
            leftIconStyles={{
              width: logoutIc.width,
              height: logoutIc.height,
            }}
            header="Log Out"
            borderWidth={responsiveWidth(91.47)}
          />
        </View>
        {/* list item end here */}
      </View>
    );
  };

  useFocusEffect(
    useCallback(() => {
      dispatch(onRequestUserDetails(userId));
    }, []),
  );

  const setRenderingContent = (state) => {
    switch (state) {
      case 'PENDING':
        return renderLoader();
      case 'FULFILLED':
        return renderProfilePage(userInfo);
      default:
        return <Text>dssdsd</Text>;
    }
  };

  return setRenderingContent(_state);
}

const styles = StyleSheet.create({
  user: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: responsiveWidth(10.67),
    marginRight: responsiveWidth(4),
    marginTop: spaceVertical.small,
    marginBottom: spaceVertical.large,
  },

  avatar: {
    width: avaImg.width,
    height: avaImg.height,
    borderRadius: avaImg.width,
    overflow: 'hidden',
  },
  mainMenu: {
    marginBottom: responsiveHeight(1.05),
  },
});

NormalProfile.propTypes = {
  navigation: PropTypes.any,
  avatar: PropTypes.any,
  name: PropTypes.string,
  email: PropTypes.string,
};
