/* global require */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView, Text } from 'react-native';

import NavigationBar from '../elements/NavigationBar';

import CommonStyles from '../styles/CommonStyles';
import { settingIc } from '../styles/icon-variables';

import NormalProfile from './profile/NormalProfile';
import GuestProfile from './profile/GuestProfile';
import { useSelector } from 'react-redux';

export default function ProfileScreen(props) {
  const { navigation } = props;

  const isLoggedIn = useSelector((state) => state.loginReducer.isLoggedIn);

  console.log('isLoggedId' + isLoggedIn);

  function _handleClickSettingButton() {
    navigation.navigate('SettingsProfileScreen');
  }

  return (
    <View style={CommonStyles.container}>
      <NavigationBar
        menu
        navigation={navigation}
        rightButtons={[
          {
            key: 1,
            buttonIcon: require('../../img/icons/ic_setttings.png'),
            buttonAction: _handleClickSettingButton,
            buttonWidth: settingIc.width,
            buttonHeight: settingIc.height,
          },
        ]}
      />

      <ScrollView>
        {isLoggedIn === false ? (
          <GuestProfile
            navigation={navigation}
            avatar={require('../../img/imba/avatar.png')}
          />
        ) : (
          <NormalProfile
            navigation={props.navigation}
            avatar={require('../../img/imba/avatar.png')}
          />
        )}
      </ScrollView>
    </View>
  );
}

ProfileScreen.propTypes = {
  navigation: PropTypes.any,
};
