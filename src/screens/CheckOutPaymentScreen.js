/* global require */
import React, { Component, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Form } from 'native-base';

import CustomStepIndicator from '../lib/CustomStepIndicator';

import NavigationBar from '../elements/NavigationBar';
import Text from '../elements/Text';
import PrimeButton from '../elements/PrimeButton';
import TextInput from '../elements/TextInput';

import CommonStyles from '../styles/CommonStyles';
import {
  responsiveWidth,
  responsiveHeight,
  marginHorizontal,
  spaceVertical,
  btnHeight,
  inputHeight,
  borderRadius,
  NAV_HEIGHT,
  STATUSBAR_HEIGHT,
  deviceWidth,
  deviceHeight,
  colors,
  indicatorStyles,
} from '../styles/variables';
import { cardIc } from '../styles/icon-variables';
import { cardContainer } from '../styles/image-variables';

import { connect, useDispatch } from 'react-redux';
import { requestCreateOrder } from '@actions/ordersActions';

export default function CheckOutPaymentScreen(props) {
  const [subContLayout, setSubContLayout] = useState({
    height: null,
    width: null,
  });

  const onSubContLayout = useCallback((e) => {
    const { width, height } = e.nativeEvent.layout;
    setSubContLayout({
      width: width,
      height: height,
    });
  });

  const [card_number, setCardNumber] = useState();
  const [card_possessor, setCardPossessor] = useState('');
  const [card_cvv, setCardCVV] = useState('');
  const [card_expMont, setCardExpMonth] = useState('');
  const [card_expDate, setCardExpDate] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [methodPayment, setMethodPayment] = useState('credit');
  const dispatch = useDispatch();

  const { route } = props;
  const subContHeight = subContLayout.height;
  const btnContHeight =
    deviceHeight - (subContHeight + NAV_HEIGHT + STATUSBAR_HEIGHT);
  const btnSetting = {
    btnWidth: responsiveWidth(82.4),
    btnHeight: btnHeight,
  };
  const billing = route.params;
  const { products } = route.params;
  const labelItems = [
    {
      label: 'Facturation',
    },
    {
      label: 'Paiement',
    },
    {
      label: 'Confirmation',
    },
  ];

  return (
    <View style={CommonStyles.container}>
      <NavigationBar back navigation={props.navigation} title="CHECK OUT" />
      <ScrollView>
        <View onLayout={onSubContLayout}>
          {/* step indicator start here */}
          <View style={CommonStyles.stepIndicator}>
            <CustomStepIndicator
              stepCount={3}
              customStyles={indicatorStyles}
              currentPosition={currentPage}
              labelItems={labelItems}
            />
          </View>
          {/* step indicator end here */}

          {/* cards list start here */}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.cardList}>
              <TouchableOpacity activeOpacity={0.8} onPress={() => {}}>
                <View style={styles.inactiveCard}>
                  <Image
                    source={require('../../img/icons/ic_card.png')}
                    style={styles.cardImg}
                  />
                  <Text black regular normal>
                    Par carte de crédit
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
          {/* cards list end here */}

          {methodPayment === 'credit' && (
            <View style={styles.form}>
              <Form>
                <TextInput
                  inputHeight={inputHeight}
                  onChangeText={(possessor) => setCardPossessor(possessor)}
                  label="Nom du détenteur"
                />
                <TextInput
                  onChangeText={(card_number) => setCardNumber(card_number)}
                  inputHeight={inputHeight}
                  label="Numéro de carte"
                />
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    inputHeight={inputHeight}
                    label="CVV"
                    onChangeText={(cvv) => setCardCVV(cvv)}
                    inputStyles={{
                      flex: 0,
                      width: responsiveWidth(24.8),
                    }}
                  />
                  <TextInput
                    inputHeight={inputHeight}
                    label="Exp.Month"
                    onChangeText={(expMonth) => setCardExpMonth(expMonth)}
                    inputStyles={{
                      flex: 0,
                      width: responsiveWidth(24.8),
                    }}
                  />
                  <TextInput
                    inputHeight={inputHeight}
                    label="Exp.Date"
                    onChangeText={(expDate) => setCardExpDate(expDate)}
                    inputStyles={{
                      flex: 0,
                      width: responsiveWidth(24.8),
                    }}
                  />
                </View>
              </Form>
            </View>
          )}
        </View>

        {/* submit button start here */}
        <View
          style={[
            styles.btnCont,
            btnContHeight > btnHeight + responsiveHeight(3.9)
              ? {
                  height: btnContHeight,
                  paddingBottom: responsiveHeight(3.9),
                }
              : { marginBottom: responsiveHeight(3.9) },
          ]}>
          <PrimeButton
            navigation={props.navigation}
            setting={btnSetting}
            btnText="PAY NOW"
            onPressButton={() => {
              console.log('lineItems', products);
              let data = {
                payment_method: 'stripe',
                payment_method_title: 'Stripe Payment Transfer',
                set_paid: true,
                lineItems: products,
                billing: billing.billing,
                shipping: billing.billing,
                cardCvv: card_cvv,
                cardExpdate: card_expDate,
                cardExpmont: card_expMont,
                cardNumber: card_number,
              };

              let formData = new FormData();
              formData.append('data', JSON.stringify(data));

              dispatch(requestCreateOrder(formData));

              /*if (this.state.methodPayment === 'credit') {
                    let paymentInfo = {
                      payment_method: 'card',
                      card_detentor: this.state.card_possessor,
                      card_cvv: this.state.card_cvv,
                      card_expMonth: this.state.card_expMonth,
                      card_expDate: this.state.card_expDate,
                    };

                    this.props.navigation.navigate('CheckOutConfirmScreen', {
                      billing,
                      paymentInfo,
                      products,
                    });

                    //TODO call the dispatch for create order

                  } else {
                    this.props.navigation.navigate('CheckOutConfirmScreen', {
                      billing,
                      products,
                    });
                  }*/
            }}
          />
        </View>
        {/* submit button end here */}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  form: {
    paddingHorizontal: marginHorizontal.normal,
    paddingBottom: responsiveHeight(11.39),
  },
  cardList: {
    flexDirection: 'row',
    marginHorizontal: marginHorizontal.normal - marginHorizontal.semiSmall / 2,
    paddingTop: spaceVertical.normal,
  },
  activeCard: {
    width: cardContainer.width,
    height: cardContainer.height,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: marginHorizontal.semiSmall / 2,
    borderWidth: 1,
    borderColor: colors.black,
    borderRadius: borderRadius.normal,
  },
  inactiveCard: {
    width: cardContainer.width,
    height: cardContainer.height,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: marginHorizontal.semiSmall / 2,
    borderRadius: borderRadius.normal,
    backgroundColor: 'rgb(248,248,248)',
  },
  cardImg: {
    width: cardIc.width,
    height: cardIc.height,
    marginBottom: responsiveHeight(3.15),
  },
  btnCont: {
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});

CheckOutPaymentScreen.propTypes = {
  navigation: PropTypes.any,
};
