/* global require */
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { Modalize } from 'react-native-modalize';
import {
  ActivityIndicator,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  onAddProductToCart,
  onRemoveProductFromCart,
  onRequestSingleProductDetails,
} from '@actions/productActions';

import CustomSwiper from '../lib/CustomSwiper';

import NavigationBar from '../elements/NavigationBar';
import Text from '../elements/Text';
import PrimeButton from '../elements/PrimeButton';
import Border from '../elements/Border';

import CommonStyles from '../styles/CommonStyles';
import {
  borderRadius,
  btnHeight,
  btnWidth,
  colors,
  deviceWidth,
  fontFamily,
  fontSize,
  marginHorizontal,
  responsiveHeight,
  responsiveWidth,
  spaceVertical,
} from '../styles/variables';
import { arrDownIc, heartIc, shareIc, videoIc } from '../styles/icon-variables';

import GridCard from '../components/GridCard';
import ListItem from '../components/ListItem';
import { useDispatch, useSelector } from 'react-redux';
import dmwlogo from '../../img/dmw/logo.png';
import { ListItem as NativeBaseList, Toast } from 'native-base';

export default function SingleProductScreen(props) {
  const [relatedProducts, setRelatedProducts] = useState();
  const [assetsAreLoaded, setAssetsAreLoaded] = useState(false);
  const [showListColor, setShowListColor] = useState(false);
  const [showListSize, setShowListSize] = useState(false);
  const [selectedColor, setSelectedColor] = useState({
    value: 'COULEUR',
    index: 0,
  });
  const [selectedSize, setSelectedSize] = useState({
    value: 'TAILLE',
    index: 0,
  });
  const [selectedProduct, setSelectedProduct] = useState({
    product: {},
    color: {
      index: 0,
      value: 'COULEUR',
    },
    size: {
      index: 0,
      value: 'TAILLE',
    },
    quantity: 0,
  });
  const [selectedQuantity, setSelectedQuantity] = useState(0);
  const isLoading = useSelector((state) => state.productsReducer.isLoading);
  const isSingleEnded = useSelector(
    (state) => state.productsReducer.isSingleEnded,
  );

  const [colorList, setColorList] = useState([]);
  const product = useSelector((state) => state.productsReducer.product);

  const imgWidth = deviceWidth;
  const imgHeight = imgWidth * 1.25;

  const proImgWidth = responsiveWidth(38.4);
  const proImgHeight = proImgWidth * 1.27;

  const dispatch = useDispatch();

  const sheetRef = useRef(null);
  const _secondSheetRef = useRef(null);
  const _thirdSheetRef = useRef(null);

  const snapPoints = useMemo(() => ['25%', '50%', '90%'], []);

  const [availableQuantity, setAvailableQuantity] = useState([
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
  ]);

  // callbacks
  const handleSheetChanges = () => {
    sheetRef.current.open();
  };

  const handleSizeSheetOpen = () => {
    _secondSheetRef.current.open();
  };
  const handleQuantitySheetOpen = () => {
    _thirdSheetRef.current.open();
  };
  const btnSetting = {
    btnWidth: btnWidth.large,
    btnHeight: btnHeight,
    style: {
      alignSelf: 'center',
      marginBottom: spaceVertical.small,
    },
  };

  const detailStyles = StyleSheet.create({
    card: {
      paddingHorizontal: marginHorizontal.semiSmall,
      paddingVertical: spaceVertical.small,
    },
    header: {
      color: colors.black,
      fontSize: fontSize.small,
      fontFamily: fontFamily.medium,
    },
  });

  const { route, navigation } = props;
  const { proId } = route.params;

  const renderBodySize = (productsSize) => {
    return (
      <View style={styles.content}>
        {productsSize.map((attribute, index) => {
          if (attribute.name.includes('Taille')) {
            let options = attribute.options;
            console.log('attribute is ' + JSON.stringify(options));
            options.map((size, index) => {
              return (
                <NativeBaseList
                  button={true}
                  onPress={() => {
                    _selectSize(index, size);
                  }}
                  key={index}>
                  <View
                    style={[
                      styles.oval,
                      {
                        marginRight: 12,
                      },
                    ]}
                  />
                  <Text>{size}</Text>
                </NativeBaseList>
              );
            });
          }
        })}
      </View>
    );
  };

  const renderBodyColor = (productColors) => {
    return (
      <View style={styles.content}>
        {productColors.map((attribute) => {
          if (attribute.name === 'Couleur') {
            let colorsOptions = attribute.options;

            {
              colorsOptions.map((color, index) => {
                console.log('color is ' + color);
                return <Text>{color}</Text>;
              });
            }
          }
        })}
      </View>
    );
  };

  const _selectSize = (id, value) => {
    setSelectedProduct((prevState) => {
      prevState.size.value = value;
      prevState.size.index = id;
      return {
        ...prevState,
      };
    });

    _secondSheetRef.current.close();
  };

  const _selectColor = (id, color) => {
    setSelectedProduct((prevState) => {
      prevState.color.value = color;
      prevState.color.index = id;

      return {
        ...prevState,
      };
    });

    sheetRef.current.close();
  };

  const _selectQuantity = (quantity) => {
    setSelectedProduct((prevState) => {
      prevState.quantity = quantity;
      return {
        ...prevState,
      };
    });
    _thirdSheetRef.current.close();
  };

  function _handleClickListItem(id) {
    navigation.push('SingleProductScreen', { proId: id });
  }

  const renderHeader = () => (
    <View style={styles.modal__header}>
      <Text style={styles.modal__headerText}>Couleurs disponibles</Text>
    </View>
  );
  const renderHeaderSize = () => (
    <View style={styles.modal__header}>
      <Text style={styles.modal__headerText}>Tailles disponibles</Text>
    </View>
  );
  const _displayRelatedProducts = (relatedProducts) => {
    return (
      <View>
        <Text black normal mediumBold style={styles.relateProHeader}>
          Ces produits pourraient aussi vous interessez
        </Text>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={styles.horizontalList}>
            {relatedProducts.map((item, index) => (
              <GridCard
                isFavourite
                key={index}
                proInfo={item}
                cardStyles={{
                  marginHorizontal: marginHorizontal.small / 2,
                }}
                wishIconStyles={{
                  bottom: 0,
                  right: 0,
                }}
                imgWidth={proImgWidth}
                imgHeight={proImgHeight}
                onPressListItem={() => _handleClickListItem(item.id)}
                textNumberOfLines={1}
              />
            ))}
          </View>
        </ScrollView>
      </View>
    );
  };

  /*core functionnality */

  function getProductDetails(id) {
    dispatch(onRequestSingleProductDetails(id));
  }

  useEffect(() => {
    getProductDetails(proId);
  }, [proId]);

  const renderPageContent = (item) => {
    return (
      <View style={CommonStyles.container}>
        <NavigationBar
          back
          navigation={navigation}
          statusBarProps={{
            translucent: true,
            barStyle: 'dark-content',
            backgroundColor: 'transparent',
          }}
          outerContStyle={{
            zIndex: 100,
            position: 'absolute',
            top: 0,
            backgroundColor: 'transparent',
          }}
        />

        <ScrollView>
          {/* slider start here */}

          {
            <CustomSwiper
              height={imgHeight}
              dotColor={colors.white}
              autoplay
              loop={true}
              activeDotColor={colors.black}>
              {product.images.map((item, index) => (
                <Image
                  key={index}
                  source={{ uri: item.src }}
                  style={{ width: imgWidth, height: imgHeight }}
                />
              ))}
            </CustomSwiper>
          }
          {/* slider end here */}

          {/* icon group start here */}
          <View style={styles.group}>
            <View style={styles.groupItem}>
              <TouchableOpacity activeOpacity={0.6}>
                <Image
                  source={require('../../img/icons/ic_heart.png')}
                  style={{ width: heartIc.width, height: heartIc.height }}
                />
              </TouchableOpacity>
            </View>
            <Border right alignSelf="center" height={responsiveHeight(4.79)} />
            <View style={styles.groupItem}>
              <TouchableOpacity
                activeOpacity={0.6}
                onPress={() => navigation.navigate('VideoScreen')}>
                <Image
                  source={require('../../img/icons/ic_video.png')}
                  style={{ width: videoIc.width, height: videoIc.height }}
                />
              </TouchableOpacity>
            </View>
            <Border right alignSelf="center" height={responsiveHeight(4.79)} />
            <View style={styles.groupItem}>
              <TouchableOpacity activeOpacity={0.6}>
                <Image
                  source={require('../../img/icons/ic_share.png')}
                  style={{ width: shareIc.width, height: shareIc.height }}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* icon group end here */}

          {/* product info start here */}
          <View style={styles.info}>
            <Text
              black
              normal
              mediumBold
              style={{ width: responsiveWidth(51.31) }}>
              {product.name}
            </Text>
            <Text black normal bold>
              ${product.price}
            </Text>
          </View>
          {/* product info end here */}

          {/* select size, color start here */}
          <Border bottom width={responsiveWidth(91.46)} alignSelf="center" />
          <View style={styles.sizeColor}>
            <TouchableOpacity
              activeOpacity={0.6}
              style={[
                styles.sizeColorItem,
                { paddingHorizontal: marginHorizontal.small },
              ]}
              onPress={handleSheetChanges}>
              {/* <View
                style={[styles.oval, { backgroundColor: selectedColor.name }]}
              />*/}
              <Text grey normal mediumBold>
                {selectedProduct.color.value}
              </Text>
              <Image
                source={require('../../img/icons/ic_down.png')}
                style={{ width: arrDownIc.width, height: arrDownIc.height }}
              />
            </TouchableOpacity>
            <Border right height={responsiveHeight(4.79)} />
            <TouchableOpacity
              activeOpacity={0.6}
              style={[
                styles.sizeColorItem,
                { paddingHorizontal: marginHorizontal.small },
              ]}
              onPress={handleSizeSheetOpen}>
              <Text grey normal mediumBold>
                {selectedProduct.size.value}
              </Text>
              <Image
                source={require('../../img/icons/ic_down.png')}
                style={{ width: arrDownIc.width, height: arrDownIc.height }}
              />
            </TouchableOpacity>
          </View>
          <Border bottom width={responsiveWidth(91.46)} alignSelf="center" />
          {/*for qunatity*/}
          <View style={styles.sizeColor}>
            <TouchableOpacity
              activeOpacity={0.6}
              style={[
                styles.sizeColorItem,
                { paddingHorizontal: marginHorizontal.small },
              ]}
              onPress={handleQuantitySheetOpen}>
              <Text grey normal mediumBold>
                Quantité
              </Text>
              <Image
                source={require('../../img/icons/ic_down.png')}
                style={{ width: arrDownIc.width, height: arrDownIc.height }}
              />
            </TouchableOpacity>
            <Border right height={responsiveHeight(4.79)} />
            <View
              style={[
                styles.sizeColorItem,
                { paddingHorizontal: marginHorizontal.small },
              ]}>
              <Text grey normal mediumBold>
                {selectedProduct.quantity}
              </Text>
            </View>
          </View>
          <Border bottom width={deviceWidth} />
          {/* select size, color end here */}

          {/* other content start here */}
          <ListItem
            cardStyles={detailStyles.card}
            headerStyles={detailStyles.header}
            header="Information détaillés"
            borderWidth={responsiveWidth(93.6)}
          />
          <ListItem
            cardStyles={detailStyles.card}
            headerStyles={detailStyles.header}
            header="Avis"
            borderWidth={responsiveWidth(93.6)}
            onPressItem={
              () => alert('Fonctionnalité en cours de développement')
              //navigation.navigate('CommentScreen')
            }
          />
          <ListItem
            cardStyles={detailStyles.card}
            headerStyles={detailStyles.header}
            header="Trouvez ma taille"
            borderWidth={responsiveWidth(93.6)}
            onPressItem={() => navigation.navigate('SizeGuideScreen')}
          />
          {/* other content end here */}

          {/* relates products start here */}
          {
            <View style={styles.relatePro}>
              {item.related_products === null ? (
                <Text black normal mediumBold style={styles.relateProHeader}>
                  Aucun produits n'est affilié
                </Text>
              ) : (
                _displayRelatedProducts(item.related_products)
              )}
            </View>
          }
          {/* relates products end here */}

          <PrimeButton
            onPressButton={() => {
              if (!selectedColor) {
                Toast.show({
                  text: 'Veuillez selectionnez la couleur ou une taille',
                  buttonText: 'Okay',
                  type: 'danger',
                });
              } else {
                // add to cart
                setSelectedProduct((prevState) => {
                  prevState.product = item;
                  return {
                    ...prevState,
                  };
                });

                dispatch(onAddProductToCart(selectedProduct));
                Toast.show({
                  text: 'Le produit a été ajouté avec success à votre panier ',
                  buttonText: 'Okay',
                  type: 'success',
                  duration: 4500,
                });
              }
            }}
            navigation={navigation}
            setting={btnSetting}
            btnText="AJOUTER AU PANIER"
          />
        </ScrollView>

        {/* select color modal end here */}

        <Modalize ref={sheetRef} snapPoint={350} HeaderComponent={renderHeader}>
          <View style={styles.content}>
            {/* {item.colors_variations.map((variation, index) => {
              return (
                <NativeBaseList
                  button={true}
                  onPress={() => {
                    _selectColor(
                      variation.variation_id,
                      variation.variation_name,
                    );
                  }}
                  key={index}>
                  <View
                    style={[
                      styles.oval,
                      {
                        marginRight: 12,
                      },
                    ]}
                  />
                  <Text>{variation.variation_name}</Text>
                </NativeBaseList>
              );
            })}*/}
          </View>
        </Modalize>

        {/* select size modal start here */}

        <Modalize
          ref={_secondSheetRef}
          snapPoint={350}
          HeaderComponent={renderHeaderSize}>
          <View style={styles.content}>
            {item.attributes.map((attribute, index) => {
              if (attribute.name.includes('Taille')) {
                let options = attribute.options;
                console.log('attribute is ' + JSON.stringify(options));

                return options.map((size, index) => (
                  <NativeBaseList
                    button={true}
                    onPress={() => {
                      _selectSize(index, size);
                    }}
                    key={index}>
                    <View
                      style={[
                        styles.oval,
                        {
                          marginRight: 12,
                        },
                      ]}
                    />
                    <Text>{size}</Text>
                  </NativeBaseList>
                ));
              }
            })}
          </View>
        </Modalize>

        <Modalize
          ref={_thirdSheetRef}
          snapPoint={350}
          HeaderComponent={renderHeaderSize}>
          <View style={styles.content}>
            {availableQuantity.map((attribute, index) => {
              return (
                <NativeBaseList
                  button={true}
                  onPress={() => {
                    _selectQuantity(attribute);
                  }}
                  key={index}>
                  <View
                    style={[
                      styles.oval,
                      {
                        marginRight: 12,
                      },
                    ]}
                  />
                  <Text>{attribute}</Text>
                </NativeBaseList>
              );
            })}
          </View>
        </Modalize>
        {/* select size modal end here */}
      </View>
    );
  };

  const Loader = () => {
    return (
      <View style={styles.loadingContainer}>
        <Image
          source={dmwlogo}
          style={{
            width: 155,
            height: 22,
            marginBottom: 10,
          }}
        />
        <ActivityIndicator />
      </View>
    );
  };

  return (
    <View style={CommonStyles.container}>
      {!isSingleEnded ? Loader() : renderPageContent(product)}
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    paddingHorizontal: 15,
  },
  group: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: colors.lightGray,
  },
  groupItem: {
    width: deviceWidth / 3,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: spaceVertical.small,
  },
  info: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: marginHorizontal.semiSmall,
  },
  sizeColor: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  sizeColorItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: deviceWidth / 2,
    paddingVertical: spaceVertical.small,
  },
  relatePro: {
    marginTop: spaceVertical.normal,
    marginBottom: spaceVertical.small,
  },
  relateProHeader: {
    paddingBottom: spaceVertical.semiSmall,
    paddingHorizontal: marginHorizontal.small,
  },
  horizontalList: {
    flexDirection: 'row',
    marginHorizontal: marginHorizontal.small / 2,
  },
  oval: {
    width: responsiveWidth(8.53),
    height: responsiveWidth(8.53) * 0.75,
    borderRadius: borderRadius.normal,
    borderColor: 'transparent',
  },
  modal: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: 12,
    borderWidth: 1,
    borderColor: colors.lightGray,
    backgroundColor: colors.white,
  },
  sizeItem: {
    padding: 12,
  },
  modal__header: {
    paddingVertical: 15,
    marginHorizontal: 15,

    borderBottomColor: '#eee',
    borderBottomWidth: 1,
  },

  modal__headerText: {
    fontSize: 15,
    fontWeight: '200',
  },
  content__row: {
    flexDirection: 'row',
    alignItems: 'center',

    paddingVertical: 15,
    borderBottomColor: '#f9f9f9',
    borderBottomWidth: 1,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

SingleProductScreen.propTypes = {
  navigation: PropTypes.any,
};
