/* global require */
import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import {
  TextInput,
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableHighlight,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';

import Accordion from '../lib/Accordion';
import Text from '../elements/Text';
import Border from '../elements/Border';

import CommonStyles from '../styles/CommonStyles';
import {
  responsiveWidth,
  responsiveHeight,
  marginHorizontal,
  spaceVertical,
  STATUSBAR_HEIGHT,
  deviceWidth,
  colors,
  fontSize,
  fontFamily,
} from '../styles/variables';
import {
  arrRightIc,
  backIc,
  searchIc,
  minusIc,
  plusIc,
  closeBtn,
} from '../styles/icon-variables';
import { sidebarAvaImg } from '../styles/image-variables';
import { CATEGORIES } from '../static/data';
import { useSelector } from 'react-redux';

import avatarSource from '../../img/imba/ic_profile_active_3x.png';

export default function SideBar(props) {
  const { navigation } = props;

  const genders = useSelector((state) => state.categoryReducer.gendersCategory);
  const categories = useSelector(
    (state) => state.categoryReducer.fetchSidebarCategoryList,
  );
  const isLoggedIn = useSelector((state) => state.loginReducer.isLoggedIn);

  const showUser = () => {
    const userInfo = useSelector((_state) => _state.userReducer.userInfo);
    let { display_name, user_email } = userInfo;

    return (
      <TouchableHighlight
        underlayColor={colors.lightGray}
        onPress={() => {
          navigation.navigate('ProfileStack');
        }}>
        <View style={styles.userCont}>
          <View style={styles.userLeft}>
            <View style={styles.avatar}>
              <Image
                source={avatarSource}
                style={{
                  width: sidebarAvaImg.width,
                  height: sidebarAvaImg.height,
                  ...Platform.select({
                    android: {
                      borderRadius: sidebarAvaImg.width,
                    },
                  }),
                }}
              />
            </View>
            <View style={{ marginLeft: marginHorizontal.small }}>
              <Text black medium mediumBold style={{ paddingBottom: 9 }}>
                {display_name ? display_name : 'Mode invité'}
              </Text>
              <Text gray small regular>
                {user_email ? user_email : 'Mode invité'}
              </Text>
            </View>
          </View>
          <Image
            source={require('../../img/icons/ic_arrow_right.png')}
            style={{ width: arrRightIc.width, height: arrRightIc.height }}
          />
        </View>
      </TouchableHighlight>
    );
  };

  function handleLogin() {
    navigation.navigate('LoginScreen');
  }

  function _renderHeader(section, index, isActive) {
    return (
      <View style={styles.menuItem}>
        {isActive ? (
          <View
            style={[styles.activeMenuItem, { backgroundColor: colors.black }]}
          />
        ) : (
          <View
            style={[styles.activeMenuItem, { backgroundColor: 'transparent' }]}
          />
        )}
        <View style={{ flex: 1, marginRight: 16 }}>
          <Text style={styles.itemTxt}>{section.name.toUpperCase()}</Text>
        </View>
        {isActive ? (
          <Image
            source={require('../../img/icons/ic_minus.png')}
            style={{ width: minusIc.width, height: minusIc.height }}
          />
        ) : (
          <Image
            source={require('../../img/icons/ic_plus.png')}
            style={{ width: plusIc.width, height: plusIc.height }}
          />
        )}
      </View>
    );
  }

  function _renderContent(section) {
    return (
      <View>
        {section.subCategories.map((item, index) => (
          <TouchableHighlight
            key={index}
            underlayColor={colors.lightGray}
            style={styles.expandedItem}>
            <TouchableOpacity
              key={item.id}
              onPress={(event) => {
                /*navigation.navigate('ListProductsScreen', {
                  parentSlug: section.slug,
                  childSlug: item.slug,
                });*/
                console.log('item is', item);
                //alert("parent"+section.slug)
              }}>
              <Text
                gray
                small
                mediumBold
                style={{ paddingVertical: spaceVertical.small }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          </TouchableHighlight>
        ))}
      </View>
    );
  }

  function handleLogOut() {
    navigation.navigate('LoginScreen');
  }

  function renderIfAuthenticated() {
    return (
      <View>
        <TouchableHighlight
          underlayColor={colors.lightGray}
          style={styles.backBtn}
          onPress={() => {
            navigation.closeDrawer();
          }}>
          <Image
            source={require('../../img/icons/ic_back.png')}
            style={{ width: backIc.width, height: backIc.height }}
          />
        </TouchableHighlight>
        {showUser()}
        <View style={styles.textInputField}>
          <Image
            source={require('../../img/icons/search.png')}
            style={styles.searchIcon}
          />
          <TextInput
            placeholder="Search"
            style={styles.textInput}
            underlineColorAndroid="transparent"
          />
        </View>
        {/* search input end here */}

        {/* menu start here */}
        <ScrollView>
          <View style={styles.mainMenu}>
            <Accordion
              underlayColor={colors.lightGray}
              sections={categories}
              renderHeader={_renderHeader}
              renderContent={_renderContent}
            />
            <Border
              bottom
              width={responsiveWidth(89.33)}
              alignSelf={'flex-end'}
              borderStyle={{
                marginVertical: spaceVertical.small,
              }}
            />
            <TouchableHighlight
              style={styles.menuItem}
              underlayColor={colors.lightGray}
              onPress={() => navigation.navigate('CollectionScreen')}>
              <Text style={styles.itemTxt}>COLLECTION</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.menuItem}
              underlayColor={colors.lightGray}
              onPress={() => navigation.navigate('DealScreen')}>
              <Text style={styles.itemTxt}>DEAL & OFFER</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.menuItem}
              underlayColor={colors.lightGray}
              onPress={() => navigation.navigate('StoreScreen')}>
              <Text style={styles.itemTxt}>FIND A STORE</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.menuItem}
              underlayColor={colors.lightGray}
              onPress={() => navigation.navigate('MagazineStack')}>
              <Text style={styles.itemTxt}>MAGAZINE FASHION</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.menuItem}
              underlayColor={colors.lightGray}
              onPress={handleLogOut}>
              <Text style={styles.itemTxt}>LOG OUT</Text>
            </TouchableHighlight>
          </View>
        </ScrollView>
      </View>
    );
  }
  function renderIfNotAuthenticated() {
    return (
      <View>
        <TouchableHighlight
          underlayColor={colors.lightGray}
          style={styles.backBtn}
          onPress={() => {
            navigation.closeDrawer();
          }}>
          <Image
            source={require('../../img/icons/ic_back.png')}
            style={{ width: backIc.width, height: backIc.height }}
          />
        </TouchableHighlight>
        <TouchableHighlight
          underlayColor={colors.lightGray}
          onPress={() => {
            navigation.navigate('ProfileStack');
          }}>
          <View style={styles.userCont}>
            <View style={styles.userLeft}>
              <View style={styles.avatar}>
                <Image
                  source={avatarSource}
                  style={{
                    width: sidebarAvaImg.width,
                    height: sidebarAvaImg.height,
                    ...Platform.select({
                      android: {
                        borderRadius: sidebarAvaImg.width,
                      },
                    }),
                  }}
                />
              </View>
              <View style={{ marginLeft: marginHorizontal.small }}>
                <Text black medium mediumBold style={{ paddingBottom: 9 }}>
                  Mode invité
                </Text>
                <Text gray small regular>
                  Mode invité
                </Text>
              </View>
            </View>
            <Image
              source={require('../../img/icons/ic_arrow_right.png')}
              style={{ width: arrRightIc.width, height: arrRightIc.height }}
            />
          </View>
        </TouchableHighlight>
        <View style={styles.textInputField}>
          <Image
            source={require('../../img/icons/search.png')}
            style={styles.searchIcon}
          />
          <TextInput
            placeholder="Search"
            style={styles.textInput}
            underlineColorAndroid="transparent"
          />
        </View>
        {/* search input end here */}

        {/* menu start here */}
        <ScrollView>
          <View style={styles.mainMenu}>
            <Accordion
              underlayColor={colors.lightGray}
              sections={categories}
              renderHeader={_renderHeader}
              renderContent={_renderContent}
            />
            <Border
              bottom
              width={responsiveWidth(89.33)}
              alignSelf={'flex-end'}
              borderStyle={{
                marginVertical: spaceVertical.small,
              }}
            />
            <TouchableHighlight
              style={styles.menuItem}
              underlayColor={colors.lightGray}
              onPress={() => navigation.navigate('CollectionScreen')}>
              <Text style={styles.itemTxt}>COLLECTION</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.menuItem}
              underlayColor={colors.lightGray}
              onPress={() => navigation.navigate('DealScreen')}>
              <Text style={styles.itemTxt}>DEAL & OFFER</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.menuItem}
              underlayColor={colors.lightGray}
              onPress={() => navigation.navigate('StoreScreen')}>
              <Text style={styles.itemTxt}>FIND A STORE</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.menuItem}
              underlayColor={colors.lightGray}
              onPress={() => navigation.navigate('MagazineStack')}>
              <Text style={styles.itemTxt}>MAGAZINE FASHION</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.menuItem}
              underlayColor={colors.lightGray}
              onPress={handleLogOut}>
              <Text style={styles.itemTxt}>LOG OUT</Text>
            </TouchableHighlight>
          </View>
        </ScrollView>
      </View>
    );
  }
  return (
    <View style={CommonStyles.container}>
      {/* back button start here */}

      {isLoggedIn ? renderIfAuthenticated() : renderIfNotAuthenticated()}

      {/* menu end here */}
    </View>
  );
}

const styles = StyleSheet.create({
  backBtn: {
    width: closeBtn.width,
    height: closeBtn.height,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: STATUSBAR_HEIGHT,
    backgroundColor: colors.black,
  },
  userCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: marginHorizontal.normal,
    paddingRight: marginHorizontal.large,
    paddingVertical: spaceVertical.semiSmall,
  },
  userLeft: {
    flex: 1,
    flexDirection: 'row',
    marginRight: marginHorizontal.small,
  },
  avatar: {
    width: sidebarAvaImg.width,
    height: sidebarAvaImg.height,
    borderRadius: sidebarAvaImg.width,
    overflow: 'hidden',
  },
  searchIcon: {
    position: 'absolute',
    bottom: 15,
    left: marginHorizontal.semiSmall,
    width: searchIc.width,
    height: searchIc.height,
  },
  textInputField: {
    flexDirection: 'row',
    width: deviceWidth,
    height: responsiveHeight(7.49),
    borderColor: 'rgb(232,232,232)',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    backgroundColor: colors.white,
  },
  textInput: {
    width: deviceWidth,
    height: responsiveHeight(7.49),
    paddingLeft: marginHorizontal.large,
    color: colors.lightGray,
    fontSize: fontSize.small,
    fontFamily: fontFamily.regular,
  },
  mainMenu: {
    marginTop: spaceVertical.semiSmall,
    marginBottom: responsiveHeight(4.19),
  },
  menuItem: {
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: spaceVertical.small,
    paddingHorizontal: marginHorizontal.large,
    backgroundColor: 'transparent',
  },
  activeMenuItem: {
    position: 'absolute',
    left: marginHorizontal.semiSmall,
    height: responsiveWidth(1.07),
    width: responsiveWidth(1.07),
    borderRadius: 100,
  },
  itemTxt: {
    color: colors.black,
    fontSize: fontSize.small,
    fontFamily: fontFamily.medium,
  },
  expandedItem: {
    paddingHorizontal: responsiveWidth(18.1),
  },
});

SideBar.propTypes = {
  navigation: PropTypes.any,
};
