/* global require */
import React, { Component, useState, useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';

import NavigationBar from '../elements/NavigationBar';
import Text from '../elements/Text';
import Border from '../elements/Border';

import CommonStyles from '../styles/CommonStyles';
import {
  responsiveHeight,
  responsiveWidth,
  marginHorizontal,
  spaceVertical,
  deviceWidth,
  colors,
} from '../styles/variables';
import {
  searchIc,
  gridIc,
  listIc,
  wishHeartIc,
} from '../styles/icon-variables';

import { PRODUCTS } from '../static/data';

import GridCard from '../components/GridCard';
import ListCard from '../components/ListCard';
import BannerCard from '../components/BannerCard';

import ApiConstant from '../api/ApiConstants';
import Api from '../api/index';

import { useFocusEffect } from '@react-navigation/native';
import {
  onFetchGendersAndCategory,
  onFetchProductsByCategoryId,
} from '@actions/productActions';
import dmwlogo from '../../img/dmw/logo.png';

const gridImgWidth = responsiveWidth(87.15) / 2;
const gridImgHeight = gridImgWidth * 1.28;

const listImgWidth = responsiveWidth(25.6);
const listImgHeight = listImgWidth * 1.29;

export default function ListProductsScreen(props) {
  const [products, setProducts] = useState([]);
  const [listType, setListType] = useState({
    type: 'GRID',
  });
  const _listType = listType.type;
  const { route, navigation } = props;
  const { params } = route;

  const dispatch = useDispatch();

  const isRequestEnded = useSelector(
    (state) => state.productsReducer.isProductsByCategoryRequestEnded,
  );
  const fetchedProducts = useSelector(
    (state) => state.productsReducer.productsByCategory,
  );

  //get the reducer

  /**
   * Go to search screen when click search button
   */
  function _handleClickSearchButton() {
    navigation.navigate('SearchScreen');
  }

  /**
   * Render products list in type: list/grid
   *
   * @param {Object} listProducts
   */
  function renderBody(listProducts) {
    if (listType.type === 'GRID') {
      return (
        <View style={styles.grid}>
          {listProducts.map((item, index) => (
            <GridCard
              isFavourite
              key={index}
              proInfo={item}
              cardStyles={{
                marginVertical: spaceVertical.small / 2,
                marginHorizontal: marginHorizontal.small / 2,
              }}
              wishIconStyles={{
                bottom: spaceVertical.small / 2,
                right: 0,
              }}
              imgWidth={gridImgWidth}
              imgHeight={gridImgHeight}
              onPressListItem={() => _handleClickListItem(item.id)}
            />
          ))}
        </View>
      );
    } else {
      return (
        <View style={styles.list}>
          {listProducts.map((item, index) => (
            <ListCard
              hasImgIc
              key={index}
              proInfo={item}
              cardStyles={{
                marginHorizontal: marginHorizontal.small,
                marginVertical: spaceVertical.small / 2,
              }}
              rightInfoStyles={{
                flex: 1,
                justifyContent: 'space-between',
              }}
              imgWidth={listImgWidth}
              imgHeight={listImgHeight}
              subHeader={
                <Text black medium bold>
                  {item.price}
                </Text>
              }
              icon={require('../../img/icons/outline_ic_heart.png')}
              iconStyles={{
                width: wishHeartIc.width,
                height: wishHeartIc.height,
              }}
              iconContStyles={{
                bottom: spaceVertical.small,
                right: 0,
              }}
              onPressListItem={() => _handleClickListItem(item)}
            />
          ))}
        </View>
      );
    }
  }

  /**
   * Go to single product screen when click best seller item
   * @param {number} id
   */
  function _handleClickListItem(id) {
    navigation.navigate('SingleProductScreen', { proId: id });
  }

  /*start function to fetch product */

  function fetchProductListAccordingToTheId(categoryId) {
    dispatch(onFetchProductsByCategoryId(categoryId));
  }

  async function fetchProductListAccordingToParentAndChildSlug(params) {
    dispatch(onFetchGendersAndCategory(params));
  }

  /* end function to fetch product */

  const Loader = () => {
    return (
      <View style={styles.loadingContainer}>
        <Image
          source={dmwlogo}
          style={{
            width: 155,
            height: 22,
            marginBottom: 10,
          }}
        />
        <ActivityIndicator />
      </View>
    );
  };

  useEffect(() => {
    if (typeof params.parentSlug === 'undefined') {
      console.log('not from sidebar');
      console.log('params is ' + JSON.stringify(params));
      fetchProductListAccordingToTheId(params.id);
    } else {
      fetchProductListAccordingToParentAndChildSlug(params);
    }
  }, [params]);

  return (
    <View style={CommonStyles.container}>
      {!isRequestEnded ? (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Loader />
        </View>
      ) : (
        <View>
          <NavigationBar
            back
            navigation={props.navigation}
            title={'dsd' + ' ' + 'PRODUCTS'}
            rightButtons={[
              {
                key: 1,
                buttonIcon: require('../../img/icons/search.png'),
                buttonAction: _handleClickSearchButton.bind(this),
                buttonWidth: searchIc.width,
                buttonHeight: searchIc.height,
              },
            ]}
          />

          {/* filter bar start here */}
          <View style={styles.filterBar}>
            <View style={styles.barLeft}>
              <Text gray small regular>
                {fetchedProducts.length}
              </Text>
            </View>
            <View style={styles.barRight}>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.barRightListItem}
                onPress={() =>
                  setListType({
                    type: 'LIST',
                  })
                }>
                {listType.type === 'LIST' && (
                  <Image
                    source={require('../../img/icons/ic_list_active.png')}
                    style={{ width: listIc.width, height: listIc.height }}
                  />
                )}
                {listType.type !== 'LIST' && (
                  <Image
                    source={require('../../img/icons/ic_list.png')}
                    style={{ width: listIc.width, height: listIc.height }}
                  />
                )}
              </TouchableOpacity>
              <Border right height={responsiveHeight(3.6)} />
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.barRightGridItem}
                onPress={() => setListType({ type: 'GRID' })}>
                {listType.type === 'GRID' && (
                  <Image
                    source={require('../../img/icons/ic_grid_active.png')}
                    style={{ width: gridIc.width, height: gridIc.height }}
                  />
                )}
                {listType.type !== 'GRID' && (
                  <Image
                    source={require('../../img/icons/ic_grid.png')}
                    style={{ width: gridIc.width, height: gridIc.height }}
                  />
                )}
              </TouchableOpacity>
              <Border right height={responsiveHeight(7.2)} />
              <TouchableOpacity
                activeOpacity={0.6}
                onPress={() => props.navigation.navigate('FilterScreen')}>
                <Text
                  black
                  small
                  mediumBold
                  style={{ paddingHorizontal: marginHorizontal.semiSmall }}>
                  FILTRE
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* filter bar end here */}

          {/* product list start here */}
          {<ScrollView>{renderBody(fetchedProducts)}</ScrollView>}
          {/* product list end here */}
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  filterBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: responsiveHeight(7.2),
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
  },
  barLeft: {
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  barRight: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  barRightListItem: {
    justifyContent: 'center',
    paddingLeft: marginHorizontal.semiSmall,
    paddingRight: marginHorizontal.small,
  },
  barRightGridItem: {
    justifyContent: 'center',
    paddingRight: marginHorizontal.semiSmall,
    paddingLeft: marginHorizontal.small,
  },
  list: {
    marginVertical: spaceVertical.small / 2,
  },
  grid: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginVertical: spaceVertical.small / 2,
  },
});

ListProductsScreen.propTypes = {
  navigation: PropTypes.any,
};
