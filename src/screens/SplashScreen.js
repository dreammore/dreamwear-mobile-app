/* global require */
/* global setTimeout */
import React, { Component, useEffect } from 'react';
import { View, Image, StyleSheet, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import ROUTES from '@ultis/routes';

import Session from '@ultis/session';

import { deviceWidth, deviceHeight } from '../styles/variables';

import { useDispatch, useSelector } from 'react-redux';
import * as loginActions from '../actions/loginActions';

export default function SplashScreen(props){


    const  dispatch  = useDispatch();
  const isLogged = useSelector((state)=> state.loginReducer.isLoggedIn)

  const {navigation} = props


  useEffect(() => {

      dispatch(loginActions.disableLoader())

    const timer = setTimeout(() => {
      if (!isLogged) {
        navigation.navigate(ROUTES.WalkthroughScreen);
      } else {
        navigation.navigate(ROUTES.MainDrawer);
      }
    }, 3000);
    return () => clearTimeout(timer);

 });


  return (
    <View style={styles.container}>
      <Image
        source={require('../../img/imba/pixta_26291075_M.png')}
        style={{ width: deviceWidth, height: deviceHeight }}
      />
      <Image
        source={require('../../img/dmw/logo.png')}
        style={styles.image}
      />
    </View>
  );

}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    alignItems: 'center',
    flex: 1,
    backgroundColor: '#ffffff',
  },
  image: {
    position: 'absolute',
    bottom: 40,
    width: 155,
    height: 22,
  },
});

SplashScreen.propTypes = {
  navigation: PropTypes.any,
};
