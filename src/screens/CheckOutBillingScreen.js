import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Form, Toast } from 'native-base';

import CustomStepIndicator from '../lib/CustomStepIndicator';
import NavigationBar from '../elements/NavigationBar';
import Text from '../elements/Text';
import PrimeButton from '../elements/PrimeButton';
import TextInput from '../elements/TextInput';

import {
  responsiveWidth,
  responsiveHeight,
  marginHorizontal,
  spaceVertical,
  btnHeight,
  inputHeight,
  NAV_HEIGHT,
  STATUSBAR_HEIGHT,
  deviceWidth,
  deviceHeight,
  indicatorStyles,
} from '../styles/variables';
import CommonStyles from '../styles/CommonStyles';

export default class CheckOutBillingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subContLayout: {
        width: null,
        height: null,
      },
      currentPage: 0,
      name: '',
      firstName: '',
      address1: '',
      phoneNumber: '',
      email: '',
      city: '',
      state: '',
      zipcode: '',
    };
  }

  render() {
    const subContHeight = this.state.subContLayout.height;
    const btnContHeight =
      deviceHeight - (subContHeight + NAV_HEIGHT + STATUSBAR_HEIGHT);
    const btnSetting = {
      btnWidth: responsiveWidth(82.4),
      btnHeight: btnHeight,
    };

    const { route } = this.props;

    const { products } = route.params;

    const labelItems = [
      {
        label: 'Facturation',
      },
      {
        label: 'Paiement',
      },
      {
        label: 'Confirmation',
      },
    ];

    return (
      <View style={CommonStyles.container}>
        <NavigationBar
          back
          navigation={this.props.navigation}
          title="CHECK OUT"
        />
        <ScrollView>
          <View onLayout={this.onSubContLayout.bind(this)}>
            {/* step indicator start here */}
            <View style={CommonStyles.stepIndicator}>
              <CustomStepIndicator
                stepCount={3}
                customStyles={indicatorStyles}
                currentPosition={this.state.currentPage}
                labelItems={labelItems}
              />
            </View>
            {/* step indicator end here */}

            {/* form start here */}
            <View style={styles.form}>
              <Text black small semiBold>
                Facturation
              </Text>
              <Form>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    inputHeight={inputHeight}
                    label="Nom "
                    inputStyles={{
                      flex: 0,
                      width: (deviceWidth - 79) / 2,
                    }}
                    onChangeText={(name) => this.setState({ name })}
                  />
                  <TextInput
                    inputHeight={inputHeight}
                    label="Prénom"
                    inputStyles={{
                      flex: 0,
                      width: (deviceWidth - 79) / 2,
                    }}
                    onChangeText={(firstName) => this.setState({ firstName })}
                  />
                </View>

                <TextInput
                  inputHeight={inputHeight}
                  label="Address 1 (Numéro et nom de rue)"
                  onChangeText={(address1) => this.setState({ address1 })}
                />
                <TextInput
                  inputHeight={inputHeight}
                  label="Numéro de téléphone"
                  onChangeText={(phoneNumber) => this.setState({ phoneNumber })}
                />
                <TextInput
                  inputHeight={inputHeight}
                  label="Addresse de messagerie"
                  onChangeText={(email) => this.setState({ email })}
                />
                <TextInput
                  inputHeight={inputHeight}
                  label="Ville"
                  onChangeText={(city) => this.setState({ city })}
                />
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    inputHeight={inputHeight}
                    label="Pays"
                    inputStyles={{
                      flex: 0,
                      width: (deviceWidth - 79) / 2,
                    }}
                    onChangeText={(state) => this.setState({ state })}
                  />
                  <TextInput
                    inputHeight={inputHeight}
                    label="Code Postal"
                    inputStyles={{
                      flex: 0,
                      width: (deviceWidth - 79) / 2,
                    }}
                    onChangeText={(zipcode) => this.setState({ zipcode })}
                  />
                </View>
              </Form>
            </View>
            {/* form end here */}
          </View>

          {this.renderCheckOutButton(btnContHeight, btnSetting, products)}
        </ScrollView>
      </View>
    );
  }

  /**
   * Render checkout button
   */
  renderCheckOutButton(btnContHeight, btnSetting, products) {
    const {
      name,
      firstName,
      address1,
      phoneNumber,
      email,
      city,
      state,
      zipcode,
    } = this.state;

    return (
      <View
        style={[
          styles.btnCont,
          btnContHeight > btnHeight + responsiveHeight(3.9)
            ? { height: btnContHeight, paddingBottom: responsiveHeight(3.9) }
            : { marginBottom: responsiveHeight(3.9) },
        ]}>
        <PrimeButton
          navigation={this.props.navigation}
          setting={btnSetting}
          btnText="NEXT"
          onPressButton={() => {
            if (name === '') {
              Toast.show({
                text: 'Svp veuillez entrer votre nom',
                position: 'bottom',
                buttonText: 'OK',
              });
              return;
            }

            if (firstName === '') {
              Toast.show({
                text: 'Svp veuillez entrer votre prénom',
                position: 'bottom',
                buttonText: 'OK',
              });
              return;
            }
            if (address1 === '') {
              Toast.show({
                text: 'Veuillez entrer svp votre addresse',
                position: 'bottom',
                buttonText: 'OK',
              });
              return;
            }
            if (city === '') {
              Toast.show({
                text: 'Veuillez svp entrer votre ville',
                position: 'bottom',
                buttonText: 'OK',
              });
              return;
            }
            if (email === '') {
              Toast.show({
                text: 'Veuillez svp entrer votre email',
                position: 'bottom',
                buttonText: 'OK',
              });
              return;
            }
            if (phoneNumber === '') {
              Toast.show({
                text: 'Veuillez svp entrer votre numéro de téléphone',
                position: 'bottom',
                buttonText: 'OK',
              });
              return;
            }
            if (state == '') {
              Toast.show({
                text: 'Veuillez svp entrer votre pays',
                position: 'bottom',
                buttonText: 'OK',
              });
              return;
            }
            if (zipcode == '') {
              Toast.show({
                text: 'Veuillez svp entrer votre code postal',
                position: 'bottom',
                buttonText: 'OK',
              });
              return;
            }

            var billing = {
              name: name,
              firstName: firstName,
              email: email,
              phoneNumber: phoneNumber,
              address1: address1,
              zipcode: zipcode,
              state: state,
              city: city,
            };
            this.props.navigation.navigate('CheckOutPaymentScreen', {
              billing,
              products,
            });
          }}
        />
      </View>
    );
  }

  /**
   *  Get size of step indicator and form component
   */
  onSubContLayout(e) {
    this.setState({
      subContLayout: {
        width: e.nativeEvent.layout.width,
        height: e.nativeEvent.layout.height,
      },
    });
  }
}

CheckOutBillingScreen.propTypes = {
  navigation: PropTypes.any,
};

const styles = StyleSheet.create({
  form: {
    paddingHorizontal: marginHorizontal.normal,
    paddingTop: spaceVertical.normal,
    paddingBottom: responsiveHeight(11.39),
  },
  btnCont: {
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});
