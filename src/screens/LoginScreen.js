/* eslint-disable no-unused-vars */
/* global require */
import React, { Component, useState, useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, ScrollView, Image } from 'react-native';
import { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import { Tabs, Toast } from 'native-base';

import Text from '../elements/Text';

import SignIn from './Login/SignIn';
import SignUp from './Login/SignUp';
import Login from '@screens/Login';

import CommonStyles from '../styles/CommonStyles';
import {
  responsiveWidth,
  responsiveHeight,
  marginHorizontal,
  spaceVertical,
  NAV_HEIGHT,
  STATUSBAR_HEIGHT,
  deviceHeight,
  scrollableTabHeight,
  colors,
  fontFamily,
  fontSize,
  bottomTxtHeight,
} from '../styles/variables';
import Provider, { useDispatch, useSelector } from 'react-redux';
import { onLoginResponse, requestLogin } from '@actions/loginActions';

export default function LoginScreen(props) {
  const dispatch = useDispatch();
  const logoWidth = responsiveWidth(22.67);
  const logoHeight = logoWidth * 0.258;
  const [logoContLayout, setLogoContLayout] = useState({
    height: null,
    width: null,
  });

  const [signinLayout, setSigninLayout] = useState({
    height: null,
    width: null,
  });
  const [signupLayout, setSignUpLayout] = useState({
    height: null,
    width: null,
  });

  /**
   * Get size of logo container
   */

  const onLogoContLayout = useCallback((event) => {
    const { width, height } = event.nativeEvent.layout;
    setLogoContLayout({
      height: height,
      width: width,
    });
  }, []);

  /**
   * Get size of sign in form
   */
  const onSigninLayout = useCallback((e) => {
    const { width, height } = e.nativeEvent.layout;
    setSigninLayout({
      height: height,
      width: width,
    });
  }, []);

  /**
   * Get size of sign up form
   */
  const onSignupLayout = useCallback((e) => {
    const { width, height } = e.nativeEvent.layout;
    setSignUpLayout({
      height: height,
      width: width,
    });
  }, []);

  const logoContHeight = logoContLayout.height;
  const signinHeight = signinLayout.height;
  const signinBottomHeight =
    deviceHeight - (scrollableTabHeight + signinHeight + logoContHeight);

  const signupHeight = signupLayout.height;
  const signupBottomHeight =
    deviceHeight - (scrollableTabHeight + signupHeight + logoContHeight);

  const userId = useSelector((state) => state.loginReducer.id);
  const displayName = useSelector((state) => state.loginReducer.displayName);
  const isNetworkFailure = useSelector(
    (state) => state.loginReducer.isNetworkFailure,
  );
  const isEnded = useSelector((state) => state.loginReducer.isEnded);
  const message = useSelector((state) => state.loginReducer.message);

  const { navigation } = props;

  /**
   * Handle when click sign in facebook
   * Login facebook get token
   */
  const handleSignInFacebook = () => {
    navigation.navigate('MainDrawer');
  };

  /**
   * Handle when click sign in google
   * Login google get idToken, accessToken
   */
  const handleSignInGoogle = () => {
    navigation.navigate('MainDrawer');
  };

  /**
   * Handle sign in with email and password
   *
   * @param {String} email
   * @param {String} password
   */
  const handleSignInWithEmailAndPassword = (email, password) => {
    let formdata = new FormData();
    formdata.append('email', email);
    formdata.append('password', password);
    dispatch(requestLogin(formdata));
  };

  return (
    <View style={CommonStyles.container}>
      {/* logo start here */}
      <View style={styles.logo} onLayout={onLogoContLayout}>
        <Image
          source={require('img/dmw/logo.png')}
          style={{ width: logoWidth, height: logoHeight }}
        />
      </View>
      {/* logo end here */}

      <Tabs
        initialPage={0}
        renderTabBar={() => (
          <ScrollableTabBar
            style={{
              height: scrollableTabHeight,
            }}
            tabsContainerStyle={{
              height: scrollableTabHeight,
              alignItems: 'flex-end',
            }}
          />
        )}
        ref={(tabView) => {
          this.tabView = tabView;
        }}
        tabBarUnderlineStyle={{
          backgroundColor: colors.black,
        }}
        tabBarBackgroundColor={colors.white}
        tabBarActiveTextColor={colors.black}
        tabBarInactiveTextColor={colors.gray}
        tabBarTextStyle={{
          fontFamily: fontFamily.medium,
          fontSize: fontSize.medium,
        }}>
        {/* sign in form start here */}
        <ScrollView heading="CONNEXION">
          <View onLayout={onSigninLayout}>
            <SignIn
              message={message}
              displayName={displayName}
              userId={userId}
              isNetworkFailure={isNetworkFailure}
              isEnded={isEnded}
              onPressSignIn={handleSignInWithEmailAndPassword}
              onPressForgotPass={() =>
                navigation.navigate('ForgotPasswordScreen')
              }
              onPressSignUp={() => this.tabView.goToPage(1)}
            />
          </View>
          <View
            style={[
              styles.bottomCont,
              signinBottomHeight > bottomTxtHeight + spaceVertical.semiSmall
                ? {
                    height: signinBottomHeight,
                    paddingBottom: spaceVertical.semiSmall,
                  }
                : { marginBottom: spaceVertical.semiSmall },
            ]}>
            <Text
              darkGray
              normal
              regular
              style={styles.bottomTxt}
              onPress={() => navigation.navigate('MainDrawer')}>
              Se connecter en tant qu'invité
            </Text>
          </View>
        </ScrollView>
        {/* sign in form end here */}

        {/* sign up form start here */}
        <ScrollView heading="INSCRIPTION">
          <View onLayout={onSignupLayout}>
            <SignUp
              isLoading={false}
              onPressSignIn={() => this.tabView.goToPage(0)}
            />
          </View>
          <View
            style={[
              styles.bottomCont,
              signinBottomHeight > bottomTxtHeight + spaceVertical.semiSmall
                ? {
                    height: signinBottomHeight,
                    paddingBottom: spaceVertical.semiSmall,
                  }
                : { marginBottom: spaceVertical.semiSmall },
            ]}
          />
        </ScrollView>
        {/* sign up form end here */}
      </Tabs>
    </View>
  );
}

const styles = StyleSheet.create({
  logo: {
    paddingTop: NAV_HEIGHT + STATUSBAR_HEIGHT,
    paddingBottom: responsiveHeight(10.64),
    marginHorizontal: marginHorizontal.large,
  },
  bottomCont: {
    justifyContent: 'flex-end',
  },
  bottomTxt: {
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
});

LoginScreen.propTypes = {
  navigation: PropTypes.any,
};
