/* global require */
import React, { Component, useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import CustomSwiper from '../lib/CustomSwiper';

import NavigationBar from '../elements/NavigationBar';
import Text from '../elements/Text';
import PrimeButton from '../elements/PrimeButton';
import ApiConstants from '../api/ApiConstants';
import { requestlastInsertedProducts } from '@actions/productActions';
import {
  onFetchCategoriesAsGenders,
  onFetchSidebarCategoriesList,
  onFetchSubCategoriesRequest,
} from '@actions/categoryActions';

import { useFocusEffect } from '@react-navigation/native';
import CommonStyles from '../styles/CommonStyles';
import {
  responsiveWidth,
  responsiveHeight,
  marginHorizontal,
  spaceVertical,
  deviceWidth,
  colors,
  borderRadius,
} from '../styles/variables';
import { arrRightIc, searchIc, indicatorDot } from '../styles/icon-variables';

import {
  GENDER,
  PRODUCTS,
  CATEGORIES,
  INSTA_IMGS,
  SUB_CATEGORIES,
} from '../static/data';

import InstagramStores from './Home/InstagramStores';
import MainCategory from './Home/MainCategory';
import SubCategories from './Home/SubCategories';
import api from '../api/index';
import { useDispatch, useSelector } from 'react-redux';
import ROUTES from '@ultis/routes';
import { onRequestUserDetails } from '@actions/userActions';

import { createEndpoint } from '@api/ApiConstants';
import GridCard from '@components/GridCard';

const bannerWidth = deviceWidth;
const bannerHeight = bannerWidth * 0.64;

const storeImgWidth = responsiveWidth(91.47);
const storeImgHeight = storeImgWidth * 0.583;

export default function HomeScreen(props) {
  const [isTranslucent, setIsTranslucent] = useState(true);
  const [lastInserted, setLastInserted] = useState([]);
  const [barColor, setbarColor] = useState('transparent');
  const dispatch = useDispatch();

  const products = useSelector((state) => state.productsReducer.products);
  const message = useSelector((state) => state.productsReducer.message);
  const isFailed = useSelector((state) => state.productsReducer.isFailed);
  const isLoading = useSelector((state) => state.productsReducer.isLoading);
  const isGenderLoading = useSelector(
    (state) => state.productsReducer.isGenderLoading,
  );
  const isLastEnded = useSelector((state) => state.productsReducer.isLastEnded);

  const isGenderFailed = useSelector(
    (state) => state.productsReducer.isGenderFailed,
  );

  const _genderRequestState = useSelector(
    (state) => state.categoryReducer.fetchGendersRequestProcessState,
  );
  const _genderRequestErrorType = useSelector(
    (state) => state.categoryReducer.errorType,
  );
  const _isfetchGendersRequestProcessEnded = useSelector(
    (state) => state.categoryReducer.isfetchGendersRequestProcessEnded,
  );
  const genders = useSelector((state) => state.categoryReducer.gendersCategory);
  const userId = useSelector((state) => state.loginReducer.id);
  const subCategories = useSelector(
    (state) => state.categoryReducer.secondaryCategory,
  );
  const isfetchSubCategoryRequestProcessEnded = useSelector(
    (state) => state.categoryReducer.isfetchSubCategoryRequestProcessEnded,
  );

  const [genderList, setGenderList] = useState();

  const btnSetting = {
    btnWidth: responsiveWidth(64),
    btnHeight: responsiveHeight(5.99),
    style: {
      position: 'absolute',
      bottom: spaceVertical.semiSmall,
    },
  };

  const fetchLastInsertedProducts = () => {
    dispatch(requestlastInsertedProducts());
  };
  const fetchProductsGender = () => {
    dispatch(onFetchCategoriesAsGenders());
  };

  const fetchSubCategory = () => {
    dispatch(onFetchSubCategoriesRequest());
  };

  const fetchUserDetails = () => {
    dispatch(onRequestUserDetails(userId));
  };

  const fetchSideBarCategoriesList = () => {
    dispatch(onFetchSidebarCategoriesList());
  };

  useEffect(() => {
    fetchLastInsertedProducts();
    fetchProductsGender();
    fetchUserDetails();
    fetchSubCategory();
    fetchSideBarCategoriesList();
  }, []);

  /*useEffect(

  );*/

  const { navigation } = props;

  /**
   * Go to single product screen when click best seller item
   *
   * @param {int} proId
   */
  const _handleClickListItem = (proId) => {
    navigation.push('SingleProductScreen', { proId: proId });
  };

  /**
   * Go to search screen when click search button
   */
  const _handleClickSearchButton = () => {
    navigation.navigate('SearchScreen');
  };
  const isCloseToTop = ({ contentOffset }) => {
    if (contentOffset.y > 0) {
      return false;
    }
    return true;
  };

  const displayGenders = () => {
    if (!_isfetchGendersRequestProcessEnded) {
      return <ActivityIndicator />;
    } else {
      if (_genderRequestState === 1) {
        return <MainCategory navigation={navigation} categories={genders} />;
      } else {
        return (
          <View style={styles.banner_error}>
            <Text white small bold style={{ textAlign: 'center' }}>
              Erreur lors de la récupération des produits {'\n'}
              Veuillez vérifiez votre connexion internet
            </Text>
          </View>
        );
      }
    }
  };

  return (
    <View style={CommonStyles.container}>
      <NavigationBar
        menu
        navigation={navigation}
        statusBarProps={{
          translucent: isTranslucent,
          barStyle: 'dark-content',
          backgroundColor: barColor,
        }}
        outerContStyle={{
          zIndex: 9999,
          position: 'absolute',
          top: 0,
          backgroundColor: barColor,
        }}
        rightButtons={[
          {
            key: 1,
            buttonIcon: require('../../img/icons/search.png'),
            buttonAction: _handleClickSearchButton.bind(this),
            buttonWidth: searchIc.width,
            buttonHeight: searchIc.height,
          },
        ]}
      />

      <ScrollView
        scrollEventThrottle={10}
        onScroll={({ nativeEvent }) => {
          if (isCloseToTop(nativeEvent)) {
            setbarColor('transparent'), setIsTranslucent(false);
          } else {
            setIsTranslucent(true);
            setbarColor('transparent');
          }
        }}>
        {/* slider start here */}
        <View style={styles.slide}>
          <CustomSwiper
            height={bannerHeight}
            autoplay
            loop={true}
            dotColor={colors.white}
            dotStyle={{
              width: indicatorDot.width,
              height: indicatorDot.height,
            }}
            activeDotColor={colors.black}
            activeDotStyle={{
              width: indicatorDot.width,
              height: indicatorDot.height,
            }}>
            <Image
              source={require('../../img/imba/banner1.png')}
              style={styles.slideImg}
            />
            <Image
              source={require('../../img/imba/banner2.png')}
              style={styles.slideImg}
            />
            <Image
              source={require('../../img/imba/banner3.png')}
              style={styles.slideImg}
            />
          </CustomSwiper>
        </View>
        {/* slider end here */}

        {/* banner start here */}
        <View style={styles.banner}>
          <Text white small bold style={{ textAlign: 'center' }}>
            LIVRAISON PARTOUT DANS LE MONDE
          </Text>
        </View>
        {/* banner end here */}

        {/* best seller start here */}
        <View style={styles.bestSeller}>
          <View style={{ flex: 1, marginRight: 16 }}>
            <Text black medium bold>
              Derniers ajouts
            </Text>
          </View>
          <TouchableOpacity activeOpacity={0.8}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text gray normal regular>
                Voir plus
              </Text>
              <Image
                source={require('../../img/icons/ic_arrow_right.png')}
                style={{
                  width: arrRightIc.width,
                  height: arrRightIc.height,
                  marginLeft: 6,
                }}
              />
            </View>
          </TouchableOpacity>
        </View>
        {isFailed || isGenderFailed ? (
          <View style={styles.banner_error}>
            <Text white small bold style={{ textAlign: 'center' }}>
              Erreur lors de la récupération des produits {'\n'}
              Veuillez vérifiez votre connexion internet
            </Text>
          </View>
        ) : isLoading || !isLastEnded ? (
          <ActivityIndicator />
        ) : (
          /*afficher les produits*/
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.horizontalList}>
              {products.map((item, index) => (
                <GridCard
                  isLoading={isLoading}
                  key={index}
                  isFavourite
                  proInfo={item}
                  cardStyles={{
                    marginHorizontal: marginHorizontal.small / 2,
                  }}
                  wishIconStyles={{
                    bottom: 0,
                    right: 0,
                  }}
                  imgWidth={deviceWidth / 2.7}
                  imgHeight={(deviceWidth / 2.7) * 1.278}
                  textNumberOfLines={1}
                  onPressListItem={() => _handleClickListItem(item.id)}
                />
              ))}
            </View>
          </ScrollView>
        )}

        {/* best seller end here */}

        {displayGenders()}

        {!isfetchSubCategoryRequestProcessEnded ? (
          <ActivityIndicator />
        ) : (
          <SubCategories
            navigation={navigation}
            subcategories={subCategories}
          />
        )}

        <InstagramStores instaImgs={INSTA_IMGS} />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  banner: {
    marginHorizontal: marginHorizontal.small,
    marginTop: spaceVertical.small,
    marginBottom: spaceVertical.semiSmall,
    paddingVertical: spaceVertical.small / 2,
    backgroundColor: colors.black,
  },
  banner_error: {
    marginHorizontal: marginHorizontal.small,
    marginTop: spaceVertical.small,
    marginBottom: spaceVertical.semiSmall,
    paddingVertical: spaceVertical.small / 2,
    backgroundColor: colors.red,
  },
  slideImg: {
    width: bannerWidth,
    height: bannerHeight,
    borderRadius: borderRadius.normal,
  },
  bestSeller: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: marginHorizontal.small,
    marginBottom: responsiveHeight(2.09),
  },
  horizontalList: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginHorizontal: marginHorizontal.small / 2,
    paddingBottom: spaceVertical.semiSmall,
  },
  locateStore: {
    position: 'relative',
    alignItems: 'center',
    marginBottom: spaceVertical.normal,
  },
  locateStoreImg: {
    width: storeImgWidth,
    height: storeImgHeight,
    borderRadius: borderRadius.normal,
  },
});

HomeScreen.propTypes = {
  navigation: PropTypes.any,
};
