/* global require */
import React, { Component, useState } from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import { Form, Toast } from 'native-base';
import Root from 'native-base';
import PropTypes from 'prop-types';
import { Provider, useDispatch, useSelector } from 'react-redux';
import * as loginActions from '../../actions/loginActions';
import * as registerActions from '../../actions/registerActions';
import configureStore from './../../store/configureStore';
import ApiClient from 'api/ApiClient';

import Text from '../../elements/Text';
import PrimeButton from '../../elements/PrimeButton';
import TextInput from '../../elements/TextInput';
//connect the store
const { store } = configureStore();

import {
  btnWidth,
  btnHeight,
  inputHeight,
  marginHorizontal,
  spaceVertical,
  responsiveWidth,
  responsiveHeight,
} from '../../styles/variables';
import { userIc, passIc, mailIc } from '../../styles/icon-variables';
import ApiConstants from '@api/ApiConstants';

export default function SignUp(props) {
  const [email, setEmail] = useState('');
  const dispatch = useDispatch();
  const userId = useSelector((state) => state.authReducer.id);
  const message = useSelector((state) => state.authReducer.message);
  const isProcessing = useSelector((state) => state.authReducer.isProcessing);
  const isRegistered = useSelector((state) => state.authReducer.isRegistered);
  const isErrored = useSelector((state) => state.authReducer.isErrored);

  const [emailError, setEmailError] = useState(false);

  const btnSetting = {
    btnWidth: btnWidth.normal,
    btnHeight: btnHeight,
  };

  const { isLoading, navigation } = props;

  if (isLoading) {
    return (
      <View style={[styles.container, styles.loading]}>
        <ActivityIndicator />
      </View>
    );
  }

  if (isProcessing) {
    return (
      <View style={[styles.container, styles.loading]}>
        <ActivityIndicator />
      </View>
    );
  }

  const displayErrorToast = () => {
    Toast.show({
      text: message,
      buttonText: 'Okay',
      type: 'danger',
    });
  };
  const displaySuccessToast = () => {
    Toast.show({
      text: 'inscription effectué avec successs',
      buttonText: 'Okay',
      type: 'success',
    });
  };

  if (isErrored) {
    displayErrorToast();
    dispatch(registerActions.resetRegistering());
  }

  if (isRegistered === true) {
    displaySuccessToast();
    dispatch(registerActions.resetRegistering());
    return (
      <View style={styles.container}>
        <Text
          black
          normal
          regular
          style={{
            marginBottom: spaceVertical.semiSmall,
            textDecorationLine: 'underline',
            textAlign: 'center',
          }}>
          Votre inscription s'est déroulé avec succès et Un E-mail de
          confirmation contenant votre mot de passe a été envoyé à {email}.
          Veuillez vous en servir pour vous connecter
        </Text>
      </View>
    );
  }

  const handleSignUp = (email) => {
    if (email == '') {
      setEmailError(true);
    } else {
      console.log('userId' + message);
      setEmail(email);
      const formdata = new FormData();
      formdata.append('email', email);
      dispatch(registerActions.requestSignUp(formdata));
    }
  };

  return (
    <View style={styles.container}>
      <Form style={styles.form}>
        <TextInput
          inputHeight={inputHeight}
          error={emailError}
          onChangeText={(email) => {
            setEmail(email), setEmailError(false);
          }}
          label="Email"
          secureTextEntry={false}
          leftIcon={require('../../../img/icons/ic_mail.png')}
          leftIconStyles={{
            width: mailIc.width,
            height: mailIc.height,
          }}
        />
      </Form>

      <View style={styles.btnCont} />

      <View style={styles.btnCont}>
        {!isProcessing ? (
          <PrimeButton
            navigation={navigation}
            setting={btnSetting}
            btnText="INSCRIPTION"
            onPressButton={() => handleSignUp(email)}
          />
        ) : (
          <ActivityIndicator />
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: marginHorizontal.large,
  },
  form: {
    paddingTop: 16,
    paddingBottom: responsiveHeight(6),
  },
  btnCont: {
    alignItems: 'center',
    paddingBottom: responsiveHeight(8.4),
  },
});

SignUp.propTypes = {
  navigation: PropTypes.any,
  onPressSignUp: PropTypes.any,
  isLoading: PropTypes.bool,
  signUpUser: PropTypes.any,
};
