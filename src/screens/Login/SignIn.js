/* global require */
import React, { Component, useState } from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import { Form, Toast } from 'native-base';
import PropTypes from 'prop-types';

import Text from '../../elements/Text';
import PrimeButton from '../../elements/PrimeButton';
import TextInput from '../../elements/TextInput';
import { onLoginProcessreseting } from '@actions/loginActions';

import {
  btnWidth,
  btnHeight,
  inputHeight,
  marginHorizontal,
  spaceVertical,
  responsiveWidth,
  responsiveHeight,
  colors,
} from '../../styles/variables';

import { userIc, passIc } from '../../styles/icon-variables';
import { useDispatch } from 'react-redux';

export default function SignIn(props) {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [errorEmail, setErrorEmail] = useState();
  const [errorPassword, setErrorPassword] = useState();
  const signInBtnSetting = {
    btnWidth: btnWidth.normal,
    btnHeight: btnHeight,
    style: {
      marginBottom: spaceVertical.semiSmall,
    },
  };

  const dispatch = useDispatch();

  const signUpBtnSetting = {
    btnWidth: btnWidth.normal,
    btnHeight: btnHeight,
    backgroundColor: colors.blue,
    borderColor: colors.blue,
  };

  const {
    onPressSignIn,
    onPressForgotPass,
    navigation,
    userId,
    displayName,
    isEnded,
    isNetworkFailure,
    message,
  } = props;

  const displaySuccessConnectToast = (message) => {
    Toast.show({
      text: 'Bon retour parmis nous ' + message,
      buttonText: 'Okay',
      type: 'success',
    });
  };

  if (isEnded) {
    if (isNetworkFailure) {
      console.log('message is frm ' + message);

      if (message !== '') {
        Toast.show({
          text: message,
          buttonText: 'Ok',
          type: 'danger',
        });
      } else {
        Toast.show({
          text:
            'Erreur de connexion! Veuillez vérifier votre connexion internet   ',
          buttonText: 'Ok',
          type: 'danger',
        });
      }

      const timer = setTimeout(() => {
        dispatch(onLoginProcessreseting());
      }, 1000);
      return () => clearTimeout(timer);
    } else {
      //ÅdisplaySuccessConnectToast(displayName);
    }
  } else {
    return (
      <View style={[styles.container, styles.loading]}>
        <ActivityIndicator />
      </View>
    );
  }

  const handleOnPressSignIn = () => {
    let isError = false;
    if (email === '' || email === undefined) {
      setErrorEmail(true);
      isError = true;
    }
    if (password === '' || email === undefined) {
      setErrorPassword(true);
      isError = true;
    }
    if (isError) {
      return;
    }
    onPressSignIn(email, password);
  };

  return (
    <View style={styles.container}>
      <Form style={styles.form}>
        <TextInput
          inputHeight={inputHeight}
          error={errorEmail}
          onChangeText={(email) => {
            setEmail(email);
            setErrorEmail(false);
          }}
          label="Nom d'utilisateur"
          leftIcon={require('../../../img/icons/ic_user.png')}
          leftIconStyles={{
            width: userIc.width,
            height: userIc.height,
          }}
        />
        <TextInput
          inputHeight={inputHeight}
          error={errorPassword}
          onChangeText={(password) => {
            setPassword(password);
            setErrorPassword(false);
          }}
          label="Mot de passe"
          leftIcon={require('../../../img/icons/ic_lock.png')}
          leftIconStyles={{
            width: passIc.width,
            height: passIc.height,
          }}
        />
      </Form>
      <View style={styles.btnCont}>
        <PrimeButton
          navigation={navigation}
          setting={signInBtnSetting}
          btnText="SE CONNECTER"
          onPressButton={handleOnPressSignIn}
        />
      </View>
      {/*TODO readd recover password  */}
      {/* <View style={{ paddingBottom: responsiveHeight(11.54) }}>
        <Text
          black
          normal
          regular
          style={{ textDecorationLine: 'underline' }}
          onPress={onPressForgotPass}>
          Mot de passe oublié?
        </Text>
      </View>*/}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: marginHorizontal.large,
  },
  form: {
    paddingTop: spaceVertical.small,
    paddingBottom: responsiveHeight(6),
  },
  btnCont: {
    alignItems: 'center',
    paddingBottom: spaceVertical.semiSmall,
  },
});

SignIn.propTypes = {
  navigation: PropTypes.any,
  onPressSignIn: PropTypes.any,
  onPressSignUp: PropTypes.any,
  onPressForgotPass: PropTypes.any,
  onPressFacebook: PropTypes.any,
  onPressGoogle: PropTypes.any,
  isLoading: PropTypes.bool,
};
