import React, { Component, useEffect } from 'react';
import { StyleSheet, View, ScrollView, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { Icon } from 'native-base';

import NavigationBar from '../elements/NavigationBar';
import Text from '../elements/Text';
import Border from '../elements/Border';
import PrimeButton from '../elements/PrimeButton';

import CommonStyles from '../styles/CommonStyles';
import {
  responsiveHeight,
  responsiveWidth,
  marginHorizontal,
  spaceVertical,
  btnHeight,
  deviceWidth,
  colors,
  fontSize,
  fontFamily,
} from '../styles/variables';
import { cartProImg } from '../styles/image-variables';
import { products } from '../static/data';
import CartCard from '../components/CartCard';
import { useSelector } from 'react-redux';

export default function CartScreen(props) {
  const { navigation } = props;

  const products = useSelector((state) => state.productsReducer.cartItems);
  let totalPrice = 0;

  const btnSetting = {
    btnWidth: responsiveWidth(42.67),
    btnHeight: btnHeight,
  };

  products.map((item) => {
    if (typeof item.product.price !== 'undefined') {
      let productPrice = parseFloat(item.product.price) * item.quantity;
      totalPrice += productPrice;
    }
  });

  function _handleClickContinue() {
    navigation.navigate('ListProductsScreen', {
      header: 'FOR WOMAN',
      image: require('../../img/imba/img_banner_woman.png'),
      count: 113,
    });
  }

  return (
    <View style={CommonStyles.container}>
      <NavigationBar menu navigation={navigation} title="CART" />
      <ScrollView>
        {/* header start here */}
        <View style={styles.headerCont}>
          <Text white mediumBold semiLarge style={styles.header}>
            VOTRE PANIER D'ACHAT
          </Text>
          <Text
            normal
            regular
            style={{ color: '#c8c8c8', textAlign: 'center' }}>
            {products == null || products.length <= 0
              ? 'No item'
              : 'Review ' + products.length + ' items '}

            {products != null && products.length > 0 && (
              <Text white mediumBold semiLarge>
                ${totalPrice}
              </Text>
            )}
          </Text>
        </View>
        {/* header end here */}

        {/* if products in cart exit */}
        {products != null && products.length > 0 && (
          <View>
            {/* products list start here */}
            <View style={styles.list}>
              {products.map((item, index) => (
                <CartCard
                  key={index}
                  cartProInfo={item}
                  cardStyles={{
                    marginLeft: marginHorizontal.small,
                    marginRight: responsiveWidth(4.8),
                    marginVertical: spaceVertical.semiSmall / 2,
                  }}
                  imgWidth={cartProImg.width}
                  imgHeight={cartProImg.height}
                  quantity={2}
                />
              ))}
            </View>
            <Border
              bottom
              width={responsiveWidth(93.6)}
              alignSelf={'flex-end'}
            />
            {/* products list end here */}

            {/* price start here */}
            <View style={styles.priceCont}>
              <View style={styles.priceItem}>
                <Text style={styles.priceTxt}>Free Shipping</Text>
                <Text style={styles.priceTxt}>$16.99</Text>
              </View>
              <View style={styles.priceItem}>
                <Text style={styles.priceTxt}>Sub Total</Text>
                <Text style={styles.priceTxt}>${totalPrice}</Text>
              </View>
              <View style={styles.priceItem}>
                <Text black normal regular>
                  Total
                </Text>
                <Text black normal bold>
                  ${totalPrice}
                </Text>
              </View>
            </View>
            <Border
              bottom
              width={responsiveWidth(93.6)}
              alignSelf={'flex-end'}
            />
            {/* price end here */}

            {/* bottom button start here */}
            <View style={styles.bottomCont}>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.bottomTxtCont}
                onPress={_handleClickContinue}>
                <View style={styles.bottomTxtInnerCont}>
                  <Icon name="ios-arrow-back" style={styles.bottomIcon} />
                  <View style={{ flex: 1 }}>
                    <Text black small regular numberOfLines={1}>
                      Continue Shopping
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
              <PrimeButton
                navigation={navigation}
                setting={btnSetting}
                btnText="PROCEDER AU PAIEMENT"
                onPressButton={() =>
                  navigation.navigate('CheckOutBillingScreen', {
                    products,
                  })
                }
              />
            </View>
            {/* bottom button end here */}
          </View>
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  headerCont: {
    width: deviceWidth,
    paddingTop: spaceVertical.semiSmall,
    paddingBottom: responsiveHeight(2.1),
    backgroundColor: colors.black,
  },
  header: {
    marginBottom: 9,
    textAlign: 'center',
  },
  list: {
    marginVertical: spaceVertical.semiSmall / 2,
  },
  priceCont: {
    marginVertical: spaceVertical.semiSmall / 2,
    marginHorizontal: marginHorizontal.semiSmall,
  },
  priceItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: spaceVertical.semiSmall / 2,
  },
  priceTxt: {
    color: colors.gray,
    fontSize: fontSize.normal,
    fontFamily: fontFamily.regular,
  },
  bottomCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: spaceVertical.semiSmall,
    marginHorizontal: marginHorizontal.semiSmall,
  },
  bottomTxtCont: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 6,
  },
  bottomTxtInnerCont: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bottomIcon: {
    marginRight: marginHorizontal.small / 2,
    color: colors.black,
    fontSize: fontSize.semiLarge,
  },
});

CartScreen.propTypes = {
  navigation: PropTypes.any,
};
