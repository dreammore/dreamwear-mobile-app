import createReducer from './../lib/createReducer';
import * as types from './../actions/types';
import { log } from 'react-native-reanimated';

const initialState = {
  products: [],
  genders: [],
  isLoading: false,
  isGenderLoading: false,
  isFailed: false,
  isGenderFailed: false,
  message: '',
  isLastEnded: false,
  isGenderEnded: false,
  isSingleEnded: false,
  isEnd: false,
  product: {},
  _productByCategoryRequestState: 0,
  _categoryProduct: [],
  productsByCategory: [],
  isProductsByCategoryRequestEnded: true,
  _productsByGenderRequestState: 0,
  errorType: '',
  cartItems: [],

  /*for allProducts*/
  shopProducts: [],
  fetchShopProductsEnded: true,
  fetchShopProductsHasFailed: false,
  error: '',
};

export const productsReducer = createReducer(initialState, {
  [types.REQUEST_LAST_INSERTED_PRODUCT](state, action) {
    return {
      ...state,
      isLoading: true,
      products: [],
      isLastEnded: false,
    };
  },
  [types.REQUEST_LAST_INSERTED_PRODUCT_RESPONSE](state, action) {
    return {
      ...state,
      isLoading: false,
      products: action.response,
      isFailed: false,
    };
  },
  [types.REQUEST_LAST_INSERTED_PRODUCT_FAILED](state, action) {
    return {
      ...state,
      isLoading: false,
      isFailed: false,
      products: [],
    };
  },
  [types.REQUEST_LAST_INSERTED_PRODUCT_END](state, action) {
    return {
      ...state,
      isLastEnded: true,
    };
  },
  /*single product details*/
  [types.REQUEST_SINGLE_PRODUCT_DETAILS](state, action) {
    return {
      ...state,
      isLoading: true,
      isFailed: false,
      isSingleEnded: false,
      product: {},
    };
  },
  /*single product details response */
  [types.REQUEST_SINGLE_PRODUCT_DETAILS_RESPONSE](state, action) {
    return {
      ...state,
      isLoading: false,
      isFailed: false,
      product: action.response,
    };
  },
  /*single product details failed */
  [types.REQUEST_SINGLE_PRODUCT_DETAILS_FAILED](state, action) {
    return {
      ...state,
      isLoading: false,
      isFailed: true,
      product: {},
    };
  },
  /*single product details end */
  [types.REQUEST_SINGLE_PRODUCT_DETAILS_END](state, action) {
    return {
      ...state,
      isLoading: false,
      isSingleEnded: true,
    };
  },

  /*START FETCHING THE PRODUCT BY GENDER*/

  [types.FETCH_PRODUCT_BY_CATEGORY_REQUEST](state, action) {
    return {
      ...state,
      isProductsByCategoryRequestEnded: false,
      productsByCategory: [],
    };
  },
  //get the response
  [types.FETCH_PRODUCT_BY_CATEGORY_REQUEST_RESPONSE](state, action) {
    return {
      ...state,
      _productByCategoryRequestState: 1,
      productsByCategory: action.response,
    };
  },

  [types.FETCH_PRODUCT_BY_CATEGORY_REQUEST_FAILED](state, action) {
    return {
      ...state,
      productsByCategory: [],
    };
  },
  [types.FETCH_PRODUCT_BY_CATEGORY_REQUEST_END](state, action) {
    return {
      ...state,
      isProductsByCategoryRequestEnded: true,
    };
  },

  /*start getting the product by slug */

  [types.FETCH_PRODUCTS_BY_GENDER_AND_SUBCATEGORY_REQUEST](state, action) {
    return {
      ...state,
      productsByCategory: [],
      isProductsByCategoryRequestEnded: false,
      errorType: '',
    };
  },

  [types.FETCH_PRODUCTS_BY_GENDER_AND_SUBCATEGORY_REQUEST_RESPONSE](
    state,
    action,
  ) {
    console.log(action.response);
    return {
      ...state,
      productsByCategory: action.response,
      errorType: '',
    };
  },

  [types.FETCH_PRODUCTS_BY_GENDER_AND_SUBCATEGORY_REQUEST_FAILED](
    state,
    action,
  ) {
    return {
      ...state,
      productsByCategory: [],
      errorType: action.errorType,
    };
  },
  [types.FETCH_PRODUCTS_BY_GENDER_AND_SUBCATEGORY_REQUEST_ENDED](
    state,
    action,
  ) {
    return {
      ...state,
      isProductsByCategoryRequestEnded: true,
    };
  },

  /*CREATE REDUCER FOR SHOP */

  [types.FETCH_PRODUCT_FOR_SHOP_REQUEST](state, action) {
    return {
      ...state,
      fetchShopProductsEnded: false,
      fetchShopProductsHasFailed: false,
    };
  },
  [types.FETCH_PRODUCT_FOR_SHOP_REQUEST_RESPONSE](state, action) {
    return {
      ...state,
      shopProducts: action.response,
      fetchShopProductsHasFailed: false,
    };
  },
  [types.FETCH_PRODUCT_FOR_SHOP_REQUEST_FAILED](state, action) {
    return {
      ...state,
      fetchShopProductsHasFailed: true,
      error: action.error,
    };
  },
  [types.FETCH_PRODUCT_FOR_SHOP_REQUEST_END](state, action) {
    return {
      ...state,
      fetchShopProductsEnded: true,
    };
  },

  [types.ADD_PRODUCT_TO_CART](state, action) {
    return {
      ...state,
      cartItems: state.cartItems.concat([action.product]),
    };
  },
  [types.REMOVE_PRODUCT_FROM_CART](state, action) {
    return {
      ...state,
      cartItems: state.cartItems.splice(
        state.cartItems.indexOf(action.product),
        1,
      ),
    };
  },
});
