import createReducer from '../lib/createReducer';
import * as types from '@actions/types';

const initialState = {
  orderId: 0,
};

export const orderReducer = createReducer(initialState, {
  [types.CREATE_ORDER_REQUEST](state, data) {
    return {
      ...state,
      data: data,
    };
  },
  [types.CREATE_ORDER_REQUEST_RESPONSE](state, action) {
    return {
      ...state,
      orderId: action.id,
    };
  },
});
