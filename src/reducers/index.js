/*
 * combines all th existing reducers
 */
import * as loadingReducer from './loadingReducer';
import * as loginReducer from './loginReducer';
import * as themeReducer from './themeReducer';
import * as authReducer from './authReducer';
import * as productReducer from './productReducer';
import * as userReducer from './userReducer';
import * as categoryReducer from '@reducers/categoryReducer';
import * as orderReducer from '@reducers/ordersReducer';

export default Object.assign(
  loginReducer,
  loadingReducer,
  themeReducer,
  authReducer,
  categoryReducer,
  productReducer,
  userReducer,
  orderReducer,
);
