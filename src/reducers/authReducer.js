import createReducer from '../lib/createReducer';
import * as types from '@actions/types';

const initialState = {
  isProcessing: false,
  id: 0,
  message: '',
  isRegistered: false,
  isErrored: false,
};

export const authReducer = createReducer(initialState, {
  [types.REGISTER_REQUEST](state, action) {
    return {
      ...state,
      id: action.id,
      user: action.user,
    };
  },
  [types.REGISTER_RESPONSE](state, action) {
    console.log('action is' + JSON.stringify(action));
    return {
      ...state,
      id: action.id,
      message: action.message,
      isProcessing: false,
      isRegistered: true,
    };
  },

  [types.REGISTER_ENABLE_LOADER](state, action) {
    console.log('action is ' + JSON.stringify(action));
    return {
      ...state,
      isProcessing: true,
    };
  },
  [types.REGISTER_DISABLE_LOADER](state, action) {
    console.log('action is ' + JSON.stringify(action));
    return {
      ...state,
      isProcessing: false,
    };
  },

  [types.REGISTER_FAILED](state, action) {
    console.log('action is' + action.message);
    return {
      ...state,
      message: action.message,
      isProcessing: false,
      isRegistered: false,
      isErrored: true,
    };
  },
  [types.LOGIN_REQUEST](state, action) {
    return {
      ...state,
      id: action.id,
    };
  },
  [types.REGISTER_RESTART](state, action) {
    return {
      ...state,
      isErrored: false,
      message: ''
    };
  },
});
