import createReducer from '@app/lib/createReducer';
import * as types from '@actions/types';

const initialState = {
  fetchGendersRequestProcessState: 0,
  isfetchGendersRequestProcessEnded: false,
  isfetchSubCategoryRequestProcessEnded: false,
  fetchSidebarCategoryList: [],
  isFetchSidebarCategoryListRequestEnded: true,
  isFetchSidebarCategoryListRequestFailed: false,
  secondaryCategory: [],
  gendersCategory: [],
  errorType: '',
};

export const categoryReducer = createReducer(initialState, {
  /*handle subcategory fetch*/
  /*init the request and process state is equal to zero
   * event : enable the loader
   */
  [types.FETCH_CATEGORIES_AS_GENDER_REQUEST](state, action) {
    return {
      ...state,
      gendersCategory: [],
      secondaryCategory: [],
      errorType: '',
      isEnded: false,
    };
  },

  /*
   * the request went well and we got the response that we passe to the staee
   * fetchGendersRequestProcessState gone to 1  that precise that the request is a success
   * */
  [types.FETCH_CATEGORIES_AS_GENDER_REQUEST_RESPONSE](state, action) {
    return {
      ...state,
      fetchGendersRequestProcessState: 1,
      gendersCategory: action.response,
      errorType: '',
    };
  },

  /*
   * the request failed and we got the response error which will be displayed to user
   * fetchGendersRequestProcessState gone to 2  that  we got a issue with the server
   */

  [types.FETCH_CATEGORIES_AS_GENDER_REQUEST_FAILED](state, action) {
    return {
      ...state,
      fetchGendersRequestProcessState: 2,
      gendersCategory: [],
      errorType: action.errorType,
    };
  },

  /*
   * Regardless of the result of the request end
   * and the fetchGendersRequestProcessStateState pass to 3
   */

  [types.FETCH_CATEGORIES_AS_GENDER_REQUEST_END](state, action) {
    return {
      ...state,
      isfetchGendersRequestProcessEnded: true,
    };
  },

  /*typs fetch subcategories */

  [types.FETCH_SUBCATEGORIES_REQUEST](state, action) {
    return {
      ...state,
      isfetchSubCategoryRequestProcessEnded: false,
      secondaryCategory: [],
    };
  },

  [types.FETCH_SUBCATEGORIES_REQUEST_RESPONSE](state, action) {
    return {
      ...state,
      secondaryCategory: action.response,
    };
  },
  [types.FETCH_SUBCATEGORIES_REQUEST_FAILED](state, action) {
    return {
      ...state,
      secondaryCategory: [],
    };
  },
  [types.FETCH_SUBCATEGORIES_REQUEST_END](state, action) {
    return {
      ...state,
      isfetchSubCategoryRequestProcessEnded: true,
    };
  },

  /*sidebar categories list request */

  [types.FETCH_SIDEBAR_CATEGORIES_LIST_REQUEST](state, action) {
    return {
      ...state,
      isFetchSidebarCategoryListRequestEnded: false,
      fetchSidebarCategoryList: [],
      isFetchSidebarCategoryListRequestFailed: false,
    };
  },
  [types.FETCH_SIDEBAR_CATEGORIES_LIST_RESPONSE](state, action) {
    return {
      ...state,
      fetchSidebarCategoryList: action.response,
      isFetchSidebarCategoryListRequestFailed: false,
    };
  },
  [types.FETCH_SUBCATEGORIES_REQUEST_FAILED](state, action) {
    return {
      ...state,
      fetchSidebarCategoryList: [],
      isFetchSidebarCategoryListRequestFailed: true,
    };
  },
  [types.FETCH_SIDEBAR_CATEGORIES_LIST_END](state, action) {
    return {
      ...state,
      isfetchGendersRequestProcessEnded: true,
    };
  },
});
