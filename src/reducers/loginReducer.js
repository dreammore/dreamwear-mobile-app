/* Login Reducer
 * handles login states in the app
 */
import createReducer from './../lib/createReducer';
import * as types from './../actions/types';

const initialState = {
  isLoggedIn: false,
  id: 0,
  isEnded: true,
  isLoginLoading: false,
  isNetworkFailure: false,
  displayName: '',
  message: '',
};

export const loginReducer = createReducer(initialState, {
  [types.LOGIN_REQUEST](state, action) {
    return {
      ...state,
      id: action.id,
      isNetworkFailure: false,
      isEnded: false,
      message: '',
    };
  },
  [types.LOGIN_RESPONSE](state, action) {
    return {
      ...state,
      id: action.response.ID,
      isLoggedIn: true,
      isNetworkFailure: false,
      message: '',
    };
  },

  [types.LOGIN_FAILED](state, action) {
    return {
      ...state,
      isLoggedIn: false,
      isNetworkFailure: true,
      message: action.message,
    };
  },

  [types.LOGIN_NETWORK_FAILED](state, action) {
    return {
      ...state,
      isLoggedIn: false,
      isNetworkFailure: true,
      message: '',
    };
  },
  [types.LOGIN_END](state) {
    return {
      ...state,
      isEnded: true,
    };
  },
  [types.LOGIN_PROCESS_RESET](state) {
    return {
      ...state,
      isLoggedIn: false,
      isNetworkFailure: false,
      isEnded: true,
    };
  },

  [types.LOG_OUT](state) {
    return {
      ...state,
      isLoggedIn: false,
      isNetworkFailure: false,
    };
  },
});
