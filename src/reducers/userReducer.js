import createReducer from '@app/lib/createReducer';
import * as types from '@actions/types';

const initialState = {
    userInfo : {},
    requestState:'SETTLED',
}

export  const userReducer = createReducer(initialState,{

    /*create the differente action*/
    [types.REQUEST_USER_DETAILS](state, action) {
        return {
            ...state,
            userInfo:{},
            requestState:'PENDING',
        }
    },
    [types.REQUEST_USER_DETAILS_END](state,action){
        return {
            ...state,
            requestState:'SETTLED',
        }
    },
    [types.REQUEST_USER_DETAILS_RESPONSE](state,action){
        return {
            ...state,
            userInfo:action.response.data,
            requestState:'FULFILLED',
        }
    },
    [types.REQUEST_USER_DETAILS_FAILED](state,action){
        return {
            ...state,
            response:action.response,
            requestState:'REJECTED',
        }
    },
    [types.REQUEST_USER_DETAILS_NETWORK_FAILURE](state,action){
        return {
            ...state,
            response:action.response,
            requestState:'NETWORK_REJECTED',
        }
    },
})