import AsyncStorage from '@react-native-community/async-storage';

export default class Session {
  static resetSharedPreferences = async () => {
    await AsyncStorage.clear();
  };
  static removeData = async (key) => {
    try {
      await AsyncStorage.removeItem(key);
    } catch (e) {
      console.log('error setting ' + e);
    }
  };

  static storeData = async (key, value) => {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(value));
    } catch (e) {
      console.log('error setting ' + e);
    }
  };

  static getData = async (key) => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        return JSON.parse(value);
      }
    } catch (e) {
      console.log('error getting' + e);
    }
  };
}
