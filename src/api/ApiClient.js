import HttpClient from './HttpClient';
import ApiConstants, { createEndpoint } from './ApiConstants';

export default class ApiClient extends HttpClient {
  constructor() {
    super({
      _baseURL: ApiConstants.BASE_URL,
    });
  }

  get LastInsertedproducts() {
    return {
      get: () => this.get(ApiConstants.LAST_INSERTED),
    };
  }

  user() {
    return {
      create: (email) => this.post(ApiConstants.REGISTER_USER, email),
      login: (credential) => this.post(ApiConstants.LOGIN, credential),
      profile: (id) => this.get(ApiConstants.PROFILE + id),
    };
  }

  products() {
    return {
      fetchLastInserted: () => this.get(ApiConstants.LAST_INSERTED),
      details: (id) => this.get(ApiConstants.SINGLE_PRODUCT + id),
      productsByCategoryId: (id) =>
        this.get(
          createEndpoint({
            url: 'api/shop',
            params: {
              category: id,
              products: '',
            },
          }),
        ),
      gendersOtherCategory: (parentSlug, childSlug) =>
        this.get(
          createEndpoint({
            url: 'api/shop/products',
            params: {
              genders: parentSlug,
              category: childSlug,
            },
          }),
        ),
      shop: () =>
        this.get(
          createEndpoint({
            url: ApiConstants.SHOPRODUCTS,
          }),
        ),
    };
  }
  categories() {
    return {
      fetchGenders: () => this.get(ApiConstants.CATEGORIES_AS_GENDERS_ENDPOINT),
      fetchSidebarCategoriesList: () => this.get(ApiConstants.SIDEBARCATEGORY),
      fetchSubcategories: () =>
        this.get(
          createEndpoint({
            url: ApiConstants.SUBCATEGORY,
          }),
        ),
    };
  }
  orders() {
    return {
      createOrder: (data) => this.post(ApiConstants.CREATEORDER, data),
    };
  }
}
