// General api to access data
import ApiConstants from './ApiConstants';

export default function api(path, params, method) {
  let options;

  if (method == !'GET' || method == !'HEAD') {
    options = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: method,
      ...(params && { body: JSON.stringify(params) }),
    };
  } else {
    options = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: method,
    };
  }

  return fetch(ApiConstants.BASE_URL + path, options)
    .then((resp) => resp.json())
    .then((json) => json)
    .catch((error) => error);
}
