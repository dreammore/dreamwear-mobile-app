/* App config for apis
 */
const ApiConstants = {
  BASE_URL_: 'http://localhost:8000/',
  BASE_URL: 'http://api.dreamwear.co/',
  LAST_INSERTED: 'api/products/lastInserted',
  CATEGORIES_AS_GENDERS_ENDPOINT: 'api/shop/categories/genders/',
  SINGLE_PRODUCT: 'api/product/',
  REGISTER_USER: 'api/user/register',
  LOGIN: 'api/user/login',
  PROFILE: 'api/user/profile/',
  SUBCATEGORY: 'api/shop/categories/secondary',
  SIDEBARCATEGORY: 'api/shop/categories/sidebar',
  SHOPRODUCTS: 'api/shop/products',
  CREATEORDER: 'api/shop/createOrder',
};

export default ApiConstants;

/*fonction permettant de creer un endpoint de façon dynamique
 * etant donné que la facon actuelle ne permet pas de rajouter des parametres
 * */
export function createEndpoint(endpoint) {
  const { url, params } = endpoint;
  let _finalEndpoint = '';
  if (typeof params === 'undefined') {
    _finalEndpoint = _finalEndpoint.concat(url);
  } else {
    _finalEndpoint = _finalEndpoint.concat(url);
    for (let key in params) {
      _finalEndpoint = _finalEndpoint.concat('/');
      _finalEndpoint = _finalEndpoint.concat(key + '/');
      _finalEndpoint = _finalEndpoint.concat(params[key]);
    }
  }
  return _finalEndpoint;
}
