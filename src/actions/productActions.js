import * as types from '@actions/types';

export function requestlastInsertedProducts() {
  return {
    type: types.REQUEST_LAST_INSERTED_PRODUCT,
  };
}

export function onRequestLastInsertedProductsResponse(response) {
  return {
    type: types.REQUEST_LAST_INSERTED_PRODUCT_RESPONSE,
    response,
  };
}
export function onRequestLastInsertedProductsEnd() {
  return {
    type: types.REQUEST_LAST_INSERTED_PRODUCT_END,
  };
}

export function onRequestLastInsertedProductsResponseFailed(response) {
  return {
    type: types.REQUEST_LAST_INSERTED_PRODUCT_FAILED,
    response,
  };
}

export function enableLoader() {
  return {
    type: types.ENABLE_LOADER,
  };
}

export function disableLoader() {
  return {
    type: types.DISABLE_LOADER,
  };
}

/*singleProduct details */

export function onRequestSingleProductDetails(id) {
  return {
    type: types.REQUEST_SINGLE_PRODUCT_DETAILS,
    id,
  };
}

export function onRequestSingleProductDetailsResponse(response) {
  return {
    type: types.REQUEST_SINGLE_PRODUCT_DETAILS_RESPONSE,
    response,
  };
}

export function onRequestSingleProductDetailsFailed(response) {
  return {
    type: types.REQUEST_SINGLE_PRODUCT_DETAILS_FAILED,
    response,
  };
}

export function onRequestSingleProductDetailsEnd() {
  return {
    type: types.REQUEST_SINGLE_PRODUCT_DETAILS_END,
  };
}

/**/

export function onFetchGendersAndCategory(action) {
  return {
    type: types.FETCH_PRODUCTS_BY_GENDER_AND_SUBCATEGORY_REQUEST,
    action,
  };
}

export function onFetchGendersAndCategoryResponse(response) {
  return {
    type: types.FETCH_PRODUCTS_BY_GENDER_AND_SUBCATEGORY_REQUEST_RESPONSE,
    response,
  };
}

export function onFetchGendersAndCategoryFailed(errorType) {
  return {
    type: types.FETCH_PRODUCTS_BY_GENDER_AND_SUBCATEGORY_REQUEST_FAILED,
    errorType,
  };
}

export function onFetchGendersAndCategoryEnded() {
  return {
    type: types.FETCH_PRODUCTS_BY_GENDER_AND_SUBCATEGORY_REQUEST_ENDED,
  };
}

export function onFetchProductsByCategoryId(id) {
  return {
    type: types.FETCH_PRODUCT_BY_CATEGORY_REQUEST,
    id,
  };
}

export function onFetchProductsByCategoryIdResponse(response) {
  return {
    type: types.FETCH_PRODUCT_BY_CATEGORY_REQUEST_RESPONSE,
    response,
  };
}

export function onFetchProductsbyCategoryIdFailed(errorType) {
  return {
    type: types.FETCH_PRODUCT_BY_CATEGORY_REQUEST_FAILED,
    errorType,
  };
}

export function onFetchProductsByCategoryEnded() {
  return {
    type: types.FETCH_PRODUCT_BY_CATEGORY_REQUEST_END,
  };
}

/*product shop */

export function onFetchShopProductsRequest() {
  return {
    type: types.FETCH_PRODUCT_FOR_SHOP_REQUEST,
  };
}

export function onFetchShopProductsRequestResponse(response) {
  return {
    type: types.FETCH_PRODUCT_FOR_SHOP_REQUEST_RESPONSE,
    response,
  };
}

export function onFetchShopProductsRequestFailed(error) {
  return {
    type: types.FETCH_PRODUCT_FOR_SHOP_REQUEST_FAILED,
    error,
  };
}

export function onFetchShopProductsRequestEnded() {
  return {
    type: types.FETCH_PRODUCT_FOR_SHOP_REQUEST_END,
  };
}

export function onAddProductToCart(product) {
  return {
    type: types.ADD_PRODUCT_TO_CART,
    product,
  };
}
export function onRemoveProductFromCart(product) {
  return {
    type: types.REMOVE_PRODUCT_FROM_CART,
    product,
  };
}
