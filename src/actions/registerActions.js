/*
 * Reducer actions related with authentication process
 */
import * as types from '@actions/types';

export function requestSignUp(data) {
  return {
    type: types.REGISTER_REQUEST,
    data,
  };
}

export function resetRegistering() {
  return {
    type: types.REGISTER_RESTART,
  };
}

export function onRegisterFailed(message) {
  return {
    type: types.REGISTER_FAILED,
    message,
  };
}

export function onRegisterResponse(id, message) {
  return {
    type: types.REGISTER_RESPONSE,
    id,
    message,
  };
}

export function enableLoader() {
  return {
    type: types.REGISTER_ENABLE_LOADER,
  };
}

export function disableLoader() {
  return {
    type: types.REGISTER_DISABLE_LOADER,
  };
}
