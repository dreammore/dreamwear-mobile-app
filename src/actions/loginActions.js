/*
 * Reducer actions related with login
 */

import * as types from '@actions/types';

export function requestLogin(credential) {
  return {
    type: types.LOGIN_REQUEST,
    credential,
  };
}

export function onLoginFailed(message) {
  return {
    type: types.LOGIN_FAILED,
    message,
  };
}

export function onLoginResponse(response) {
  return {
    type: types.LOGIN_RESPONSE,
    response,
  };
}

export function onLoginNetworkFailure() {
  return {
    type: types.LOGIN_NETWORK_FAILED,
  };
}

export function onLoginRequestEnd() {
  return {
    type: types.LOGIN_END,
  };
}

export function enableLoader() {
  return {
    type: types.LOGIN_ENABLE_LOADER,
  };
}

export function disableLoader() {
  return {
    type: types.LOGIN_DISABLE_LOADER,
  };
}

export function onLoginProcessreseting() {
  return {
    type: types.LOGIN_PROCESS_RESET,
  };
}

export function logOut() {
  return {
    type: types.LOG_OUT,
  };
}
