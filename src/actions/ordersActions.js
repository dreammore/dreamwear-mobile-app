import * as types from '@actions/types';

export function requestCreateOrder(data) {
  return {
    type: types.CREATE_ORDER_REQUEST,
    data,
  };
}

export function requestCreateOrderResponse(response) {
  return {
    type: types.CREATE_ORDER_REQUEST_RESPONSE,
    response,
  };
}

export function requestCreateOrderFailed(error) {
  return {
    type: types.CREATE_ORDER_REQUEST_FAILED,
    error,
  };
}

export function requestCreateOrderEnded() {
  return {
    type: types.CREATE_ORDER_REQUEST_END,
  };
}
