import * as types from '@actions/types';

export function enableLoader() {
  return {
    type: types.ENABLE_LOADER,
  };
}

export function disableLoader() {
  return {
    type: types.DISABLE_LOADER,
  };
}
/*fetch products gender */

export function onFetchCategoriesAsGenders() {
  return {
    type: types.FETCH_CATEGORIES_AS_GENDER_REQUEST,
  };
}
export function onFetchCategoriesAsGendersResponse(response) {
  return {
    type: types.FETCH_CATEGORIES_AS_GENDER_REQUEST_RESPONSE,
    response,
  };
}
export function onFetchCategoriesAsGendersFailed(errorType) {
  return {
    type: types.FETCH_CATEGORIES_AS_GENDER_REQUEST_FAILED,
    errorType,
  };
}
export function onFetchCategoriesAsGendersEnded() {
  return {
    type: types.FETCH_CATEGORIES_AS_GENDER_REQUEST_END,
  };
}

export function onFetchSubCategoriesRequest() {
  return {
    type: types.FETCH_SUBCATEGORIES_REQUEST,
  };
}

export function onFetchSubCategoriesRequestResponse(response) {
  return {
    type: types.FETCH_SUBCATEGORIES_REQUEST_RESPONSE,
    response,
  };
}
export function onFetchSubCategoriesRequestResponseEmpty(response) {
  return {
    type: types.FETCH_SUBCATEGORIES_REQUEST_RESPONSE_EMPTY,
    response,
  };
}

export function onFetchSubCategoriesResponseFailed(errorType) {
  return {
    type: types.FETCH_SUBCATEGORIES_REQUEST_FAILED,
    errorType,
  };
}

export function onFetchSubCategoriesRequestEnded() {
  return {
    type: types.FETCH_SUBCATEGORIES_REQUEST_END,
  };
}

/*sidebar categories list */
export function onFetchSidebarCategoriesList() {
  return {
    type: types.FETCH_SIDEBAR_CATEGORIES_LIST_REQUEST,
  };
}

export function onFetchSideBarCategoriesListResponse(response) {
  return {
    type: types.FETCH_SIDEBAR_CATEGORIES_LIST_RESPONSE,
    response,
  };
}
export function onFetchSidebarCategoriesListRequestFailed() {
  return {
    type: types.FETCH_SIDEBAR_CATEGORIES_LIST_FAILED,
  };
}

export function onFetchSidebarCategoriesListRequestEnded() {
  return {
    type: types.FETCH_SIDEBAR_CATEGORIES_LIST_END,
  };
}
