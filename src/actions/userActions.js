import * as types from '@actions/types';

export function onRequestUserDetailsEnableLoader(){
    return {
        type:types.REQUEST_USER_DETAILS_ENABLE_LOADER
    }
}

export function onRequestUserDetailsDisableLoader(){
    return {
        type:types.REQUEST_USER_DETAILS_DISABLE_LOADER
    }
}

export function onRequestUserDetails(userId){
    return {
        type: types.REQUEST_USER_DETAILS,
        userId
    }
}

export function onRequestUserDetailsResponse(response){
    return {
        type: types.REQUEST_USER_DETAILS_RESPONSE,
        response
    }
}

export function onRequestUserDetailsFailed(failure){
    return {
        type : types.REQUEST_USER_DETAILS_FAILED,
        failure
    }
}
export function onRequestUserDetailsNetworkFailed(failure){
    return {
        type : types.REQUEST_USER_DETAILS_NETWORK_FAILURE,
        failure
    }
}

export function onRequestUserDetailsEnded(){
    return {
        type: types.REQUEST_USER_DETAILS_END
    }
}