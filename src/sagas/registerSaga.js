import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { Alert } from 'react-native';

import * as registerActions from '@actions/registerActions';
import ApiClient from 'api/ApiClient';

let apiInstance = new ApiClient();

export default function* regiserAsync(action) {
  yield put(registerActions.enableLoader());

  try {
    const response = yield call(apiInstance.user().create, action.data);
    if (response.success === false) {
      yield put(registerActions.disableLoader());
      yield put(registerActions.onRegisterFailed(response.data.message));

    } else {
      yield put(registerActions.disableLoader());
      console.log('response is '+JSON.stringify(response))
      yield put(
        registerActions.onRegisterResponse(
          response.data.id,
          response.data.message,
        ),
      );

    }
  } catch (e) {
    console.log(e);
  }

  //call the registerApi
}
