/* Redux saga class
 * logins the user into the app
 * requires username and password.
 * un - username
 * pwd - password
 */
import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import { Alert } from 'react-native';

import * as loginActions from './../actions/loginActions';
import ApiClient from 'api/ApiClient';

let apiInstance = new ApiClient();

// Our worker Saga that logins the user
export default function* loginAsync(action) {
  try {
    const _loginRequest = yield call(
      apiInstance.user().login,
      action.credential,
    );
    if (_loginRequest.code === 500) {
      yield put(loginActions.onLoginNetworkFailure());
    }
    if (_loginRequest.success === false) {
      yield put(loginActions.onLoginFailed(_loginRequest.data.message));
    }
    if (_loginRequest.success === true) {
      yield put(loginActions.onLoginResponse(_loginRequest.data));
    }
  } catch (e) {
    console.log('network response' + e);
    yield put(loginActions.onLoginNetworkFailure(e.message()));
  } finally {
    yield put(loginActions.onLoginRequestEnd());
  }
}
