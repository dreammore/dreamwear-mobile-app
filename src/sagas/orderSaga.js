import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import * as categoriesActions from './../actions/ordersActions';
import ApiClient from 'api/ApiClient';

let apiInstance = new ApiClient();
export function* createOrderAsync(action) {
  try {
    //creer et retourner le order id
    console.log('data is ', action.data);
    let createOrder = yield call(apiInstance.orders().createOrder, action.data);
  } catch (e) {
    console.error('error from creating', e);
  }
}
