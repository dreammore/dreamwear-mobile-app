import ApiClient from '@api/ApiClient';
import { put, call, select } from 'redux-saga/effects';
let apiInstance = new ApiClient();
import * as userAction from './../actions/userActions';

// Our worker Saga that logins the user
export function* userDetailsAsync(action) {
  yield put(userAction.onRequestUserDetailsEnableLoader());

  try {
    const userRequest = yield call(apiInstance.user().profile, action.userId);
    if (userRequest.success === true) {
      yield put(userAction.onRequestUserDetailsResponse(userRequest.data.user));
    } else {
      yield put(userAction.onRequestUserDetailsNetworkFailed(userRequest.data));
    }
  } catch (e) {
    yield put(userAction.onRequestUserDetailsNetworkFailed(e));
  }
}
