import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import * as categoriesActions from './../actions/categoryActions';
import ApiClient from 'api/ApiClient';

let apiInstance = new ApiClient();

export function* fetchCategoriesAsGenderAsync(action) {
  try {
    const response = yield call(apiInstance.categories().fetchGenders);
    if (response.length !== 0) {
      yield put(categoriesActions.onFetchCategoriesAsGendersResponse(response));
    } else {
      yield put(
        categoriesActions.onFetchCategoriesAsGendersFailed('API_FAILURE'),
      );
    }
  } catch (e) {
    yield put(
      categoriesActions.onFetchCategoriesAsGendersFailed('NETWORK_FAILURE'),
    );
  }
  yield put(categoriesActions.onFetchCategoriesAsGendersEnded());
}

export function* fetchSubCategoriesAsync(action) {
  try {
    const response = yield call(apiInstance.categories().fetchSubcategories);
    yield put(categoriesActions.onFetchSubCategoriesRequestResponse(response));
  } catch (e) {
    yield put(categoriesActions.onFetchCategoriesAsGendersFailed('e'));
  } finally {
    yield put(categoriesActions.onFetchSubCategoriesRequestEnded());
  }
}

export function* fetchSideBarCategoriesAsync(action) {
  try {
    const response = yield call(
      apiInstance.categories().fetchSidebarCategoriesList,
    );
    yield put(categoriesActions.onFetchSideBarCategoriesListResponse(response));
  } catch (e) {
    yield put(categoriesActions.onFetchSidebarCategoriesListRequestFailed());
  } finally {
    yield put(categoriesActions.onFetchSidebarCategoriesListRequestEnded());
  }
}
