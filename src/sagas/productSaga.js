import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { Alert } from 'react-native';

import * as productsActions from '@actions/productActions';
import ApiClient from 'api/ApiClient';

let apiInstance = new ApiClient();

export function* fetchLastInsertedProductAsync() {
  yield put(productsActions.enableLoader());

  try {
    const response = yield call(apiInstance.products().fetchLastInserted);
    yield put(productsActions.disableLoader());
    yield put(productsActions.onRequestLastInsertedProductsResponse(response));
    yield put(productsActions.onRequestLastInsertedProductsEnd());
  } catch (e) {
    console.log('error is ' + e);
    yield put(productsActions.disableLoader());
    yield put(
      productsActions.onRequestLastInsertedProductsResponseFailed(e.message),
    );
    yield put(productsActions.onRequestLastInsertedProductsEnd());
  }
  yield put(productsActions.disableLoader());

  //call the registerApi
}

export function* fetchProductsDetailsAync(action) {
  yield put(productsActions.enableLoader());
  try {
    const response = yield call(apiInstance.products().details, action.id);
    yield put(productsActions.onRequestSingleProductDetailsResponse(response));
  } catch (e) {
    console.log('exception is' + e);
    yield put(productsActions.disableLoader());
    yield put(productsActions.onRequestSingleProductDetailsFailed(e.message));
  } finally {
    yield put(productsActions.onRequestSingleProductDetailsEnd());
  }
}

export function* fetchProductsByCategory(action) {
  try {
    const response = yield call(
      apiInstance.products().productsByCategoryId,
      action.id,
    );
    yield put(productsActions.onFetchProductsByCategoryIdResponse(response));
  } catch (e) {
    yield put(productsActions.onFetchProductsbyCategoryIdFailed(e.name));
  } finally {
    yield put(productsActions.onFetchProductsByCategoryEnded());
  }
}

export function* fetchProductsByGendersAndCategory(action) {
  try {
    const response = yield call(
      apiInstance.products().gendersOtherCategory,
      action.action.parentSlug,
      action.action.childSlug,
    );

    console.log('response' + JSON.stringify(response));
    if (response.length > 0) {
      yield put(productsActions.onFetchGendersAndCategoryResponse(response));
    } else {
      yield put(productsActions.onFetchGendersAndCategoryFailed('bof'));
    }
  } catch (e) {
    yield put(productsActions.onFetchGendersAndCategoryFailed(e.message));
  } finally {
    yield put(productsActions.onFetchGendersAndCategoryEnded());
  }
}

export function* fetchShopProducts() {
  try {
    const response = yield call(apiInstance.products().shop);

    console.log('products is' + JSON.stringify(response));

    if (response.length > 0) {
      yield put(productsActions.onFetchShopProductsRequestResponse(response));
    } else {
      yield put(productsActions.onFetchShopProductsRequestFailed('respons'));
    }
  } catch (e) {
    yield put(productsActions.onFetchShopProductsRequestFailed(e));
  } finally {
    yield put(productsActions.onFetchShopProductsRequestEnded());
  }
}
