/**
 *  Redux saga class init
 */
import { takeEvery, all, takeLatest, fork } from 'redux-saga/effects';
import * as types from '../actions/types';
import loginSaga from './loginSaga';
import registerSaga from './registerSaga';
import {
  fetchLastInsertedProductAsync,
  fetchProductsDetailsAync,
  fetchProductsByGendersAndCategory,
  fetchProductsByCategory,
  fetchShopProducts,
} from './productSaga';
import {
  fetchCategoriesAsGenderAsync,
  fetchSideBarCategoriesAsync,
  fetchSubCategoriesAsync,
} from './categorySaga';
import { userDetailsAsync } from './userSaga';
import { onFetchSidebarCategoriesList } from '@actions/categoryActions';
import { createOrderAsync } from './orderSaga';

export default function* watch() {
  yield all([
    takeLatest(types.REGISTER_REQUEST, registerSaga),
    takeLatest(types.LOGIN_REQUEST, loginSaga),
    takeLatest(types.REQUEST_SINGLE_PRODUCT_DETAILS, fetchProductsDetailsAync),
    takeLatest(
      types.FETCH_PRODUCTS_BY_GENDER_AND_SUBCATEGORY_REQUEST,
      fetchProductsByGendersAndCategory,
    ),
    takeLatest(types.FETCH_PRODUCT_FOR_SHOP_REQUEST, fetchShopProducts),
    takeLatest(
      types.FETCH_PRODUCT_BY_CATEGORY_REQUEST,
      fetchProductsByCategory,
    ),
    takeEvery(
      types.REQUEST_LAST_INSERTED_PRODUCT,
      fetchLastInsertedProductAsync,
    ),
    takeEvery(
      types.FETCH_CATEGORIES_AS_GENDER_REQUEST,
      fetchCategoriesAsGenderAsync,
    ),
    takeEvery(types.REQUEST_USER_DETAILS, userDetailsAsync),
    takeEvery(types.FETCH_SUBCATEGORIES_REQUEST, fetchSubCategoriesAsync),
    takeEvery(
      types.FETCH_SIDEBAR_CATEGORIES_LIST_REQUEST,
      fetchSideBarCategoriesAsync,
    ),
    takeLatest(types.CREATE_ORDER_REQUEST, createOrderAsync),
  ]);
}
